package cafe.client;

public class Console {
    public static native void write(String message) /*-{
        console.log(message);
    }-*/;
}
