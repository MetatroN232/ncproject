package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ChangeDishesEvent extends GwtEvent<ChangeDishesEventHandler> {
    public static Type<ChangeDishesEventHandler> TYPE = new Type<ChangeDishesEventHandler>();

    public Type<ChangeDishesEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(ChangeDishesEventHandler handler) {
        handler.onChangeDishes(this);
    }
}
