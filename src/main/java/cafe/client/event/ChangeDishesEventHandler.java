package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ChangeDishesEventHandler extends EventHandler {
    void onChangeDishes(ChangeDishesEvent event);
}
