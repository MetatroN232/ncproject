package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ChangeDisposalsEvent extends GwtEvent<ChangeDisposalsEventHandler> {
    public static Type<ChangeDisposalsEventHandler> TYPE = new Type<ChangeDisposalsEventHandler>();

    public Type<ChangeDisposalsEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(ChangeDisposalsEventHandler handler) {
        handler.onChangeDisposals(this);
    }
}
