package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RefreshMenuListEventHandler extends EventHandler {
    void onRefreshMenuList(RefreshMenuListEvent event);
}
