package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ChangeDisposalsEventHandler extends EventHandler {
    void onChangeDisposals(ChangeDisposalsEvent event);
}
