package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ChangeCategoriesEvent extends GwtEvent<ChangeCategoriesEventHandler> {
    public static Type<ChangeCategoriesEventHandler> TYPE = new Type<>();

    public Type<ChangeCategoriesEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(ChangeCategoriesEventHandler handler) {
        handler.onChangeCategories(this);
    }
}
