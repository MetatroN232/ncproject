package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ChangeCategoriesEventHandler extends EventHandler {
    void onChangeCategories(ChangeCategoriesEvent event);
}
