package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ChangeOrdersEventHandler extends EventHandler {
    void onChangeOrders(ChangeOrdersEvent event);
}
