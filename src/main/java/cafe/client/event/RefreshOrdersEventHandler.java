package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RefreshOrdersEventHandler extends EventHandler {
    void onRefreshOrders(RefreshOrdersEvent event);
}
