package cafe.client.event;

public interface ContextClickHandler<T> {
    void onClick(T object);
}
