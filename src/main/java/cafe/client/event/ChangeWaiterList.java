package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ChangeWaiterList extends GwtEvent<ChangeWaiterListHandler> {
    public static Type<ChangeWaiterListHandler> TYPE = new Type<ChangeWaiterListHandler>();

    public Type<ChangeWaiterListHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(ChangeWaiterListHandler handler) {
        handler.onChangeWaiterList(this);
    }
}
