package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RefreshOrdersEvent extends GwtEvent<RefreshOrdersEventHandler> {
    public static Type<RefreshOrdersEventHandler> TYPE = new Type<RefreshOrdersEventHandler>();

    public Type<RefreshOrdersEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(RefreshOrdersEventHandler handler) {
        handler.onRefreshOrders(this);
    }
}
