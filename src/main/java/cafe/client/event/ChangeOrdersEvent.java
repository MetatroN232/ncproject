package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ChangeOrdersEvent extends GwtEvent<ChangeOrdersEventHandler> {
    public static Type<ChangeOrdersEventHandler> TYPE = new Type<ChangeOrdersEventHandler>();

    public Type<ChangeOrdersEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(ChangeOrdersEventHandler handler) {
        handler.onChangeOrders(this);
    }
}
