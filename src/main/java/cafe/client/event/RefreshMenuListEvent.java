package cafe.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RefreshMenuListEvent extends GwtEvent<RefreshMenuListEventHandler> {
    public static Type<RefreshMenuListEventHandler> TYPE = new Type<RefreshMenuListEventHandler>();

    public Type<RefreshMenuListEventHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(RefreshMenuListEventHandler handler) {
        handler.onRefreshMenuList(this);
    }
}
