package cafe.client.event;

public interface SubmitEventHandler<T> {
    void onSubmit(T object);
}
