package cafe.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ChangeWaiterListHandler extends EventHandler {
    void onChangeWaiterList(ChangeWaiterList event);
}
