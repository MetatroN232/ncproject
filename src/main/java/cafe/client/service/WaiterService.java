package cafe.client.service;

import cafe.shared.dto.WaiterDto;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.*;
import java.util.List;

@Path("/api")
public interface WaiterService extends RestService {

    @GET
    @Path("/waiters")
    void getWaiters(MethodCallback<List<WaiterDto>> callback);

    @POST
    @Path("/waiters")
    void createWaiter(WaiterDto waiter, MethodCallback<Void> callback);

    @PUT
    @Path("/waiters")
    void updateWaiter(WaiterDto waiter, MethodCallback<Void> callback);

    @DELETE
    @Path("/waiters")
    void deleteWaiter(WaiterDto waiter, MethodCallback<Void> callback);
}
