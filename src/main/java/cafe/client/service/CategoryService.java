package cafe.client.service;

import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.*;
import java.util.List;

@Path("/api/categories")
public interface CategoryService extends RestService {
    @GET
    void getCategories(MethodCallback<List<CategoryDto>> callback);

    @POST
    void createCategory(CategoryDto category, MethodCallback<Void> callback);

    @PUT
    void updateCategory(CategoryDto category, MethodCallback<Void> callback);

    @DELETE
    void deleteCategory(CategoryDto category, MethodCallback<Void> callback);

    @GET
    @Path("/{categoryId}")
    void getCategory(@PathParam("categoryId") int categoryId, MethodCallback<CategoryDto> callback);

    @GET
    @Path("/{categoryId}/dishes")
    void getDishByCategory(@PathParam("categoryId") int categoryId, MethodCallback<List<DishDto>> callback);

    @GET
    @Path("/{categoryId}/dishes")
    void getDishesByCategoryWithPricesByDate(@PathParam("categoryId") int categoryId,
                                             @QueryParam("date") long date,
                                             MethodCallback<List<DishDto>> callback);

}
