package cafe.client.service;

import cafe.shared.dto.DishDto;
import cafe.shared.dto.OrderDetail;
import cafe.shared.dto.OrderDto;
import cafe.shared.dto.WaiterDto;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.*;
import java.util.List;
import java.util.Map;

@Path("/api")
public interface OrderService extends RestService {
    @GET
    @Path("/orders")
    void getOrders(MethodCallback<List<OrderDto>> callback);

    @POST
    @Path("/orders")
    void createOrder(OrderDto orderDto, MethodCallback<Void> callback);

    @PUT
    @Path("/orders")
    void updateOrder(OrderDto orderDto, MethodCallback<Void> callback);

    @DELETE
    @Path("/orders")
    void deleteOrder(OrderDto orderDto, MethodCallback<Void> callback);

    @POST
    @Path("/orders/filterAndSort/{sort}")
    void getOrdersByFilterAndSort(Map<String, Object> filterMap, @PathParam("sort") String sort, MethodCallback<List<OrderDto>> callback);

    @GET
    @Path("/orders/{orderId}")
    void getOrderById(@PathParam("orderId") int orderId, MethodCallback<OrderDto> callback);

    @PUT
    @Path("/orders/{orderId}")
    void setStatus(@PathParam("orderId") int orderId, @QueryParam("status") boolean status,
                   MethodCallback<Void> callback);

    @GET
    @Path("/orders/{orderId}/details")
    void getOrderDetailsById(@PathParam("orderId") int orderId,
                             MethodCallback<List<OrderDetail>> callback);

    @GET
    @Path("/orders/{orderId}/waiter")
    void getWaiterByOrder(@PathParam("orderId") int orderId,
                          MethodCallback<WaiterDto> callback);

    @GET
    @Path("/dishes/{dishId}")
    void getDishById(@PathParam("dishId") int dishId,
                     MethodCallback<DishDto> callback);

}
