package cafe.client.service;

import cafe.shared.dto.DishDto;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.*;
import java.util.List;

@Path("/api/dishes")
public interface DishService extends RestService {

    @POST
    void createDish(DishDto dishDto, MethodCallback<Void> callback);

    @PUT
    void updateDish(DishDto dishDto, MethodCallback<Void> callback);

    @DELETE
    void deleteDish(DishDto dishDto, MethodCallback<Void> callback);

    @GET
    void findDishByName(@QueryParam("name") String name, MethodCallback<List<DishDto>> callback);

    @GET
    void getDishesOnDateByCategory(
            @QueryParam("category") int category,
            @QueryParam("date") long date,
            MethodCallback<List<DishDto>> callback
    );

}
