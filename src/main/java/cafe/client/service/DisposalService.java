package cafe.client.service;

import cafe.shared.dto.DisposalDto;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.*;
import java.util.List;

@Path("/api/disposals")
public interface DisposalService extends RestService {
    @GET
    void getDisposals(MethodCallback<List<DisposalDto>> callback);

    @POST
    void createDisposal(DisposalDto disposalDto, MethodCallback<Void> callback);

    @PUT
    void updateDisposal(DisposalDto disposalDto, MethodCallback<Void> callback);

    @DELETE
    void deleteDisposal(DisposalDto disposalDto, MethodCallback<Void> callback);
}
