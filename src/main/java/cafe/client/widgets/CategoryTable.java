package cafe.client.widgets;

import cafe.client.command.DeleteCategoryCommand;
import cafe.client.command.DeleteCategoryCommandHandler;
import cafe.client.event.ContextClickHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.shared.dto.CategoryDto;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

import static cafe.client.widgets.IconHtml.*;

public class CategoryTable extends CellTable<CategoryDto> {

    private final ContextMenu<CategoryDto> contextMenu;
    private final SingleSelectionModel<CategoryDto> selectionModel;
    private final CustomDialogBox updateDialog;

    public CategoryTable() {
        selectionModel = new SingleSelectionModel<>();
        contextMenu = new ContextMenu<>();
        updateDialog = new CustomDialogBox();

        updateDialog.setTitle("Изменение категории");
        updateDialog.setHint("Введите новое название категории:");

        contextMenu.addButton(EDIT + " Изменить", new ContextClickHandler<CategoryDto>() {
            @Override
            public void onClick(CategoryDto category) {
                updateDialog.show(category.getName());
            }
        });

        contextMenu.addButton(MINUS + " Удалить", new ContextClickHandler<CategoryDto>() {
            @Override
            public void onClick(CategoryDto category) {
                fireEvent(new DeleteCategoryCommand(category));
            }
        });

        TextColumn<CategoryDto> categoryNameColumn = new TextColumn<CategoryDto>() {
            @Override
            public String getValue(CategoryDto object) {
                return object.getName();
            }
        };


        ActionCell<CategoryDto> option = new ActionCell<CategoryDto>(new SafeHtml() {
            @Override
            public String asString() {
                return OPTION;
            }
        }, new ActionCell.Delegate<CategoryDto>() {
            @Override
            public void execute(CategoryDto category) {
                contextMenu.show(category);
            }
        }) {
            @Override
            protected void onEnterKeyDown(Context context, Element parent, CategoryDto value, NativeEvent event, ValueUpdater<CategoryDto> valueUpdater) {
                super.onEnterKeyDown(context, parent, value, event, valueUpdater);
                contextMenu.setPosition(event.getClientX(), event.getClientY());
            }
        };

        Column<CategoryDto, CategoryDto> optionColumn = new Column<CategoryDto, CategoryDto>(option) {
            @Override
            public CategoryDto getValue(CategoryDto object) {
                return object;
            }
        };

        optionColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        optionColumn.setCellStyleNames("option-button");

        addColumn(categoryNameColumn, "Категория");
        addColumn(optionColumn, "");

        setColumnWidth(optionColumn, 50, com.google.gwt.dom.client.Style.Unit.PX);

        setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
        setSelectionModel(selectionModel);
    }

    public void addChangeHandler(SelectionChangeEvent.Handler handler) {
        selectionModel.addSelectionChangeHandler(handler);
    }

    public CategoryDto getSelectedCategory() {
        return selectionModel.getSelectedObject();
    }

    public void addSubmitUpdateHandler(SubmitEventHandler<String> handler) {
        updateDialog.addSubmitHandler(handler);
    }

    public void addDeleteHandler(DeleteCategoryCommandHandler handler) {
        addHandler(handler, DeleteCategoryCommand.TYPE);
    }
}
