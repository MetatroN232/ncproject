package cafe.client.widgets;

import com.google.gwt.event.dom.client.KeyCodes;
import org.gwtbootstrap3.client.ui.IntegerBox;

public class IntegerInputBox extends IntegerBox {

    public IntegerInputBox() {
        addKeyDownHandler(event -> {
            int keyCode = event.getNativeKeyCode();

            if (event.isAnyModifierKeyDown())
                cancelKey();

            switch (keyCode) {
                case KeyCodes.KEY_LEFT:
                case KeyCodes.KEY_RIGHT:
                case KeyCodes.KEY_DELETE:
                case KeyCodes.KEY_BACKSPACE:
                case KeyCodes.KEY_ENTER:
                case KeyCodes.KEY_ESCAPE:
                case KeyCodes.KEY_TAB:
                    return;
            }

            String parsedInput = parseNumberKey(keyCode);
            if (parsedInput.length() <= 0) {
                cancelKey();
            }
        });
    }

    private String parseNumberKey(int keyCode) {
        String result = "";

        switch (keyCode) {
            case KeyCodes.KEY_ZERO:
            case KeyCodes.KEY_ONE:
            case KeyCodes.KEY_TWO:
            case KeyCodes.KEY_THREE:
            case KeyCodes.KEY_FOUR:
            case KeyCodes.KEY_FIVE:
            case KeyCodes.KEY_SIX:
            case KeyCodes.KEY_SEVEN:
            case KeyCodes.KEY_EIGHT:
            case KeyCodes.KEY_NINE:
                return String.valueOf(keyCode);

            case KeyCodes.KEY_NUM_ONE:
                return "1";
            case KeyCodes.KEY_NUM_TWO:
                return "2";
            case KeyCodes.KEY_NUM_THREE:
                return "3";
            case KeyCodes.KEY_NUM_FOUR:
                return "4";
            case KeyCodes.KEY_NUM_FIVE:
                return "5";
            case KeyCodes.KEY_NUM_SIX:
                return "6";
            case KeyCodes.KEY_NUM_SEVEN:
                return "7";
            case KeyCodes.KEY_NUM_EIGHT:
                return "8";
            case KeyCodes.KEY_NUM_NINE:
                return "9";
            case KeyCodes.KEY_NUM_ZERO:
                return "0";
        }
        return result;
    }
}
