package cafe.client.widgets;

import cafe.client.command.DeleteDishCommand;
import cafe.client.command.DeleteDishCommandHandler;
import cafe.client.event.ContextClickHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.shared.dto.DishDto;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

import static cafe.client.widgets.IconHtml.*;
import static com.google.gwt.dom.client.Style.Unit.PX;

public class DishTable extends CellTable<DishDto> {

    private final ContextMenu<DishDto> contextMenu;

    private final SingleSelectionModel<DishDto> selectionModel;

    private final CustomDialogBox updateDishDialog;

    public DishTable() {
        contextMenu = new ContextMenu<>();
        updateDishDialog = new CustomDialogBox();
        selectionModel = new SingleSelectionModel<>();

        updateDishDialog.setTitle("Изменение блюда");
        updateDishDialog.setHint("Введите новое название блюда:");

        contextMenu.addButton(EDIT + " Изменить", new ContextClickHandler<DishDto>() {
            @Override
            public void onClick(DishDto dish) {
                updateDishDialog.show(dish.getName());
            }
        });

        contextMenu.addButton(MINUS + " Удалить", new ContextClickHandler<DishDto>() {
            @Override
            public void onClick(DishDto dish) {
                fireEvent(new DeleteDishCommand(dish));
            }
        });

        setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
        setSelectionModel(selectionModel);

        initColumns();
    }

    public void addSubmitUpdateHandler(SubmitEventHandler<String> handler) {
        updateDishDialog.addSubmitHandler(handler);
    }

    public void addDeleteHandler(DeleteDishCommandHandler handler) {
        addHandler(handler, DeleteDishCommand.TYPE);
    }

    public DishDto getSelectedDish() {
        return selectionModel.getSelectedObject();
    }

    public void addChangeHandler(SelectionChangeEvent.Handler handler) {
        selectionModel.addSelectionChangeHandler(handler);
    }

    private void initColumns() {
        TextColumn<DishDto> dishNameColumn = new TextColumn<DishDto>() {
            @Override
            public String getValue(DishDto object) {
                return object.getName();
            }
        };
        TextColumn<DishDto> dishPriceColumn = new TextColumn<DishDto>() {
            @Override
            public String getValue(DishDto object) {
                return object.getPrice() == 0 ? "~" : String.valueOf(object.getPrice());
            }
        };

        ActionCell<DishDto> option = new ActionCell<DishDto>(new SafeHtml() {
            @Override
            public String asString() {
                return OPTION;
            }
        }, new ActionCell.Delegate<DishDto>() {
            @Override
            public void execute(DishDto dish) {
                contextMenu.show(dish);
            }
        }) {
            @Override
            protected void onEnterKeyDown(Context context, Element parent, DishDto value, NativeEvent event, ValueUpdater<DishDto> valueUpdater) {
                super.onEnterKeyDown(context, parent, value, event, valueUpdater);
                contextMenu.setPosition(event.getClientX(), event.getClientY());
            }
        };

        Column<DishDto, DishDto> optionColumn = new Column<DishDto, DishDto>(option) {
            @Override
            public DishDto getValue(DishDto object) {
                return object;
            }
        };

        optionColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        optionColumn.setCellStyleNames("option-button");
        dishPriceColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

        setColumnWidth(dishPriceColumn, 150, PX);
        setColumnWidth(optionColumn, 50, PX);

        addColumn(dishNameColumn, "Название");
        addColumn(dishPriceColumn, "Цена");
        addColumn(optionColumn, "");
    }
}
