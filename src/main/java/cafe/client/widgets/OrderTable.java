package cafe.client.widgets;

import cafe.client.command.*;
import cafe.client.event.ContextClickHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.client.view.OrderPreview;
import cafe.shared.dto.OrderDetail;
import cafe.shared.dto.OrderDto;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

import java.util.Date;

import static cafe.client.widgets.IconHtml.*;

public class OrderTable extends CellTable<OrderDto> {

    private final ContextMenu<OrderDto> contextMenu;
    private final OrderPreview updateDialog;

    public OrderTable() {
        this.contextMenu = new ContextMenu<>();
        this.updateDialog = new OrderPreview();

        contextMenu.addButton(EDIT + " Изменить", new ContextClickHandler<OrderDto>() {
            @Override
            public void onClick(OrderDto object) {
                updateDialog.show(object);
            }
        });

        contextMenu.addButton(MINUS + " Удалить", new ContextClickHandler<OrderDto>() {
            @Override
            public void onClick(OrderDto object) {
                fireEvent(new DeleteOrderCommand(object));
            }
        });

        contextMenu.addButton("Счет", new ContextClickHandler<OrderDto>() {
            @Override
            public void onClick(OrderDto object) {
                fireEvent(new OpenOrderCheckCommand(object));
            }
        });

        contextMenu.addButton("Закрыть/Открыть", new ContextClickHandler<OrderDto>() {
            @Override
            public void onClick(OrderDto object) {
                fireEvent(new ChangeOrderStatusCommand(object));
            }
        });

        initColumns();
    }

    public void addSubmitUpdateHandler(SubmitEventHandler<OrderDto> handler) {
        updateDialog.addSubmitHandler(handler);
    }

    public void addChangeStatusOrderHandler(ChangeOrderStatusCommandHandler handler) {
        addHandler(handler, ChangeOrderStatusCommand.TYPE);
    }

    public void addOpenCheckHandler(OpenOrderCheckCommandHandler handler) {
        addHandler(handler, OpenOrderCheckCommand.TYPE);
    }
    public void addDeleteHandler(DeleteOrderCommandHandler handler) {
        addHandler(handler, DeleteOrderCommand.TYPE);
    }

    private void initColumns() {
        TextColumn<OrderDto> orderNumberCol = new TextColumn<OrderDto>() {
            @Override
            public String getValue(OrderDto object) {
                return String.valueOf(object.getOrderId());
            }
        };
        TextColumn<OrderDto> orderDateCol = new TextColumn<OrderDto>() {
            @Override
            public String getValue(OrderDto object) {
                Date date = object.getDate();
                String dateString = DateTimeFormat.getFormat("HH:mm  dd.MM.yyyy").format(date);
                return dateString;
            }
        };
        TextColumn<OrderDto> tableNumberCol = new TextColumn<OrderDto>() {
            @Override
            public String getValue(OrderDto object) {
                return String.valueOf(object.getNumTable());
            }
        };
        TextColumn<OrderDto> waiterCol = new TextColumn<OrderDto>() {
            @Override
            public String getValue(OrderDto object) {
                return object.getWaiter().getFam() + " " + object.getWaiter().getName();
            }
        };
        TextColumn<OrderDto> totalPriceOrderCol = new TextColumn<OrderDto>() {
            @Override
            public String getValue(OrderDto object) {
                int total = 0;
                for (OrderDetail od : object.getOrderDetails()) {
                    total += od.getDish().getPrice() * od.getCount();
                }
                return String.valueOf(total);
            }
        };
        TextColumn<OrderDto> statusCol = new TextColumn<OrderDto>() {
            @Override
            public String getValue(OrderDto object) {
                return object.isActive() ? "Активен" : "Закрыт";
            }
        };


        ActionCell<OrderDto> option = new ActionCell<OrderDto>(new SafeHtml() {
            @Override
            public String asString() {
                return OPTION;
            }
        }, new ActionCell.Delegate<OrderDto>() {
            @Override
            public void execute(OrderDto order) {
                contextMenu.show(order);
            }
        }) {
            @Override
            protected void onEnterKeyDown(Context context, Element parent,
                                          OrderDto value, NativeEvent event,
                                          ValueUpdater<OrderDto> valueUpdater) {
                super.onEnterKeyDown(context, parent, value, event, valueUpdater);
                contextMenu.setPosition(event.getClientX(), event.getClientY());
            }
        };

        Column<OrderDto, OrderDto> optionColumn = new Column<OrderDto, OrderDto>(option) {
            @Override
            public OrderDto getValue(OrderDto object) {
                return object;
            }
        };

        optionColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        optionColumn.setCellStyleNames("option-button");

        addColumn(orderNumberCol, "Номер заказа");
        addColumn(orderDateCol, "Дата");
        addColumn(tableNumberCol, "Номер столика");
        addColumn(statusCol, "Статус");
        addColumn(waiterCol, "Официант");
        addColumn(totalPriceOrderCol, "Стоимость");
        addColumn(optionColumn);
    }

    public void bind() {
        updateDialog.bind();
    }
    
}
