package cafe.client.widgets;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import org.gwtbootstrap3.client.ui.DropDownMenu;

import static com.google.gwt.dom.client.BrowserEvents.CLICK;
import static com.google.gwt.dom.client.BrowserEvents.KEYDOWN;

public class DropDownCell<C> extends AbstractCell<C> {
    private final SafeHtml html;
    private DropDownMenu dropDownMenu;

    public DropDownCell(SafeHtml message) {
        super(CLICK, KEYDOWN);

        this.html = new SafeHtmlBuilder()
                .appendHtmlConstant("<div class=\"dropdown\">")
                .appendHtmlConstant("<a href=\"javascript:;\" data-toggle=\"dropdown\" aria-expanded=\"false\">")
                .append(message)
                .appendHtmlConstant("</a>")
                .appendHtmlConstant("</div>").toSafeHtml();
    }

    @Override
    public void render(Context context, C value, SafeHtmlBuilder sb) {
        sb.append(html);
    }
}
