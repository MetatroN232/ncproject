package cafe.client.widgets;

public abstract class IconHtml {
    public static final String OPTION = "<i class='fa fa-cog'></i>";
    public static final String MINUS = "<i class='fa fa-minus'></i>";
    public static final String PLUS = "<i class='fa fa-plus'></i>";
    public static final String REFRESH = "<i class='fa fa-refresh'></i>";
    public static final String EDIT = "<i class='fa fa-edit'></i>";
}
