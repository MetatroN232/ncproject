package cafe.client.widgets;

import cafe.shared.dto.DisposalDetail;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

import static com.google.gwt.dom.client.Style.Unit.PX;

public class DisposalDetailTable extends CellTable<DisposalDetail> {

    public DisposalDetailTable() {

        TextColumn<DisposalDetail> dishNameColumn = new TextColumn<DisposalDetail>() {
            @Override
            public String getValue(DisposalDetail object) {
                return object.getDish().getName();
            }
        };

        EditTextCell priceCell = new EditTextCell();
        Column<DisposalDetail, String> dishPriceColumn = new Column<DisposalDetail, String>(priceCell) {
            @Override
            public String getValue(DisposalDetail object) {
                return String.valueOf(object.getDish().getPrice());
            }
        };



                /*new TextColumn<DisposalDetail>() {
            @Override
            public String getValue(DisposalDetail object) {
                return String.valueOf(object.getDish().getPrice());
            }
        };*/

        dishPriceColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        dishPriceColumn.setFieldUpdater(new FieldUpdater<DisposalDetail, String>() {
            @Override
            public void update(int index, DisposalDetail object, String value) {
                object.getDish().setPrice(Integer.parseInt(value));
            }
        });

        setColumnWidth(dishPriceColumn, 150, PX);

        addColumn(dishNameColumn, "Название");
        addColumn(dishPriceColumn, "Цена");

    }

    private class MyCell extends EditTextCell {

        @Override
        public void onBrowserEvent(Context context, Element parent, String value,
                                   NativeEvent event, ValueUpdater<String> valueUpdater) {
            super.onBrowserEvent(context, parent, value, event, valueUpdater);
        }

        private String parseNumberKey(int keyCode) {
            String result = "";

            switch (keyCode) {
                case KeyCodes.KEY_ZERO:
                case KeyCodes.KEY_ONE:
                case KeyCodes.KEY_TWO:
                case KeyCodes.KEY_THREE:
                case KeyCodes.KEY_FOUR:
                case KeyCodes.KEY_FIVE:
                case KeyCodes.KEY_SIX:
                case KeyCodes.KEY_SEVEN:
                case KeyCodes.KEY_EIGHT:
                case KeyCodes.KEY_NINE:
                    return String.valueOf((char) keyCode);
                case KeyCodes.KEY_NUM_ZERO:
                    return "0";
                case KeyCodes.KEY_NUM_ONE:
                    return "1";
                case KeyCodes.KEY_NUM_TWO:
                    return "2";
                case KeyCodes.KEY_NUM_THREE:
                    return "3";
                case KeyCodes.KEY_NUM_FOUR:
                    return "4";
                case KeyCodes.KEY_NUM_FIVE:
                    return "5";
                case KeyCodes.KEY_NUM_SIX:
                    return "6";
                case KeyCodes.KEY_NUM_SEVEN:
                    return "7";
                case KeyCodes.KEY_NUM_EIGHT:
                    return "8";
                case KeyCodes.KEY_NUM_NINE:
                    return "9";
            }
            return result;
        }
    }

}
