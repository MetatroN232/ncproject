package cafe.client.widgets;

import cafe.shared.dto.OrderDetail;
import com.google.gwt.user.cellview.client.TextColumn;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

public class OrderDetailTable extends CellTable<OrderDetail> {

    public OrderDetailTable() {
        TextColumn<OrderDetail> dishNameCol = new TextColumn<OrderDetail>() {
            @Override
            public String getValue(OrderDetail object) {
                return object.getDish().getName();
            }
        };

        TextColumn<OrderDetail> dishCountCol = new TextColumn<OrderDetail>() {
            @Override
            public String getValue(OrderDetail object) {
                return String.valueOf(object.getCount());
            }
        };
        TextColumn<OrderDetail> dishPriceCol = new TextColumn<OrderDetail>() {
            @Override
            public String getValue(OrderDetail object) {
                return String.valueOf(object.getDish().getPrice());
            }
        };
        TextColumn<OrderDetail> dishTotalPriceCol = new TextColumn<OrderDetail>() {
            @Override
            public String getValue(OrderDetail object) {
                return String.valueOf(object.getDish().getPrice() * object.getCount());
            }
        };

        addColumn(dishNameCol, "Наименование");
        addColumn(dishCountCol, "Количество");
        addColumn(dishPriceCol, "Цена");
        addColumn(dishTotalPriceCol, "Стоимость");

    }
}
