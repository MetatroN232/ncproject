package cafe.client.widgets;

import cafe.client.command.DeleteDisposalCommand;
import cafe.client.command.DeleteDisposalCommandHandler;
import cafe.client.event.ContextClickHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.client.view.DisposalPreview;
import cafe.shared.dto.DisposalDto;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

public class DisposalTable extends CellTable<DisposalDto> {

    private final ContextMenu<DisposalDto> contextMenu;
    private final DisposalPreview updateDialog;

    public DisposalTable() {
        contextMenu = new ContextMenu<>();
        updateDialog = new DisposalPreview();

        contextMenu.addButton(IconHtml.EDIT + " Изменить", new ContextClickHandler<DisposalDto>() {
            @Override
            public void onClick(DisposalDto object) {
                updateDialog.show(object);
            }
        });

        contextMenu.addButton(IconHtml.MINUS + " Удалить", new ContextClickHandler<DisposalDto>() {
            @Override
            public void onClick(DisposalDto object) {
                fireEvent(new DeleteDisposalCommand(object));
            }
        });

        TextColumn<DisposalDto> numberCol = new TextColumn<DisposalDto>() {
            @Override
            public String getValue(DisposalDto object) {
                return String.valueOf(object.getNumDisposal());
            }
        };

        TextColumn<DisposalDto> dateCol = new TextColumn<DisposalDto>() {
            @Override
            public String getValue(DisposalDto object) {
                return DateTimeFormat.getFormat("dd.MM.yyyy").format(object.getDate());
            }
        };

        ActionCell<DisposalDto> optionCell = new ActionCell<DisposalDto>(new SafeHtml() {
            @Override
            public String asString() {
                return IconHtml.OPTION;
            }
        }, new ActionCell.Delegate<DisposalDto>() {
            @Override
            public void execute(DisposalDto object) {
                contextMenu.show(object);
            }
        }) {
            @Override
            public void onBrowserEvent(Context context, Element parent, DisposalDto value, NativeEvent event, ValueUpdater<DisposalDto> valueUpdater) {
                super.onBrowserEvent(context, parent, value, event, valueUpdater);
                contextMenu.setPosition(event.getClientX(), event.getClientY());
            }
        };

        Column<DisposalDto, DisposalDto> optionCol = new Column<DisposalDto, DisposalDto>(optionCell) {
            @Override
            public DisposalDto getValue(DisposalDto object) {
                return object;
            }
        };

        optionCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        optionCol.setCellStyleNames("option-button");

        addColumn(numberCol, "№");
        addColumn(dateCol, "Дата");
        addColumn(optionCol);
    }

    public void addDeleteDisposalHandler(DeleteDisposalCommandHandler handler) {
        addHandler(handler, DeleteDisposalCommand.TYPE);
    }

    public void addSubmitUpdateHandler(SubmitEventHandler<DisposalDto> handler) {
        updateDialog.addSubmitHandler(handler);
    }

    public void bind() {
        updateDialog.bind();
    }
}
