package cafe.client.widgets;

import cafe.client.event.SubmitEventHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

public class CustomDialogBox extends Composite {
    interface MyDialogBoxUiBinder extends UiBinder<HTMLPanel, CustomDialogBox> {
    }

    @UiField
    DialogBox dialog;

    @UiField
    VerticalPanel panel;

    @UiField
    Label label;

    @UiField
    TextBox textField;

    @UiField
    Button submit;

    @UiField
    Button cancel;


    private static MyDialogBoxUiBinder ourUiBinder = GWT.create(MyDialogBoxUiBinder.class);

    public CustomDialogBox() {
        initWidget(ourUiBinder.createAndBindUi(this));

        init();
        bind();
    }

    public void addSubmitHandler(SubmitEventHandler<String> handler) {
        submit.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                handler.onSubmit(getText());
                hide();
            }
        });
    }

    public void show() {
        textField.setText("");
        dialog.center();
    }

    public void show(String s) {
        textField.setText(s);
        dialog.center();
    }

    public void hide() {
        dialog.hide();
    }

    public void setTitle(String title) {
        dialog.setText(title);
    }

    public void setHint(String hint) {
        label.setText(hint);
    }

    public String getText() {
        return textField.getText();
    }

    private void init() {
        submit.setText("Ok");
        cancel.setText("Cancel");
    }

    private void bind() {
        cancel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dialog.hide();
            }
        });
    }



}