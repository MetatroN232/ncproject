package cafe.client.widgets;

import cafe.client.command.DeleteWaiterCommand;
import cafe.client.command.DeleteWaiterCommandHandler;
import cafe.client.command.UpdateWaiterCommand;
import cafe.client.command.UpdateWaiterCommandHandler;
import cafe.client.event.ContextClickHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.client.view.WaiterPreview;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

public class WaiterTable extends CellTable<WaiterDto> {

    private final WaiterPreview updateDialog;
    private final ContextMenu<WaiterDto> contextMenu;

    public WaiterTable() {
        updateDialog = new WaiterPreview();
        contextMenu = new ContextMenu<>();

        updateDialog.setTitle("Изменение данных официанта");

        contextMenu.addButton(IconHtml.EDIT + " Изменить", new ContextClickHandler<WaiterDto>() {
            @Override
            public void onClick(WaiterDto waiter) {
                updateDialog.show(waiter);
            }
        });

        contextMenu.addButton(IconHtml.MINUS + " Удалить", new ContextClickHandler<WaiterDto>() {
            @Override
            public void onClick(WaiterDto waiter) {
                fireEvent(new DeleteWaiterCommand(waiter));
            }
        });

        initColumns();
    }

    private void initColumns() {
        TextColumn<WaiterDto> secondNameCol = new TextColumn<WaiterDto>() {
            @Override
            public String getValue(WaiterDto object) {
                return object.getFam();
            }
        };

        TextColumn<WaiterDto> firstNameCol = new TextColumn<WaiterDto>() {
            @Override
            public String getValue(WaiterDto object) {
                return object.getName();
            }
        };

        TextColumn<WaiterDto> middleNameCol = new TextColumn<WaiterDto>() {
            @Override
            public String getValue(WaiterDto object) {
                return object.getOtch();
            }
        };


        TextColumn<WaiterDto> dateCol = new TextColumn<WaiterDto>() {
            @Override
            public String getValue(WaiterDto object) {
                return DateTimeFormat.getFormat("dd.MM.yyyy").format(object.getBirthday());
            }
        };

        TextColumn<WaiterDto> addressCol = new TextColumn<WaiterDto>() {
            @Override
            public String getValue(WaiterDto object) {
                return object.getAddress();
            }
        };

        ActionCell<WaiterDto> optionCell = new ActionCell<WaiterDto>(new SafeHtml() {
            @Override
            public String asString() {
                return IconHtml.OPTION;
            }
        }, new ActionCell.Delegate<WaiterDto>() {
            @Override
            public void execute(WaiterDto object) {
                contextMenu.show(object);
            }
        }) {
            @Override
            public void onBrowserEvent(Context context, Element parent, WaiterDto value, NativeEvent event, ValueUpdater<WaiterDto> valueUpdater) {
                super.onBrowserEvent(context, parent, value, event, valueUpdater);
                contextMenu.setPosition(event.getClientX(), event.getClientY());
            }
        };

        Column<WaiterDto, WaiterDto> optionCol = new Column<WaiterDto, WaiterDto>(optionCell) {
            @Override
            public WaiterDto getValue(WaiterDto object) {
                return object;
            }
        };
        optionCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        optionCol.setCellStyleNames("option-button");

        addColumn(secondNameCol, "Фамилия");
        addColumn(firstNameCol, "Имя");
        addColumn(middleNameCol, "Отчество");
        addColumn(dateCol, "Дата рождения");
        addColumn(addressCol, "Адрес проживания");
        addColumn(optionCol);
    }

    public void bind() {
        updateDialog.bind();
        updateDialog.addSubmitHandler(new SubmitEventHandler<WaiterDto>() {
            @Override
            public void onSubmit(WaiterDto object) {
                fireEvent(new UpdateWaiterCommand(object));
            }
        });
    }

    public void addUpdateHandler(UpdateWaiterCommandHandler handler) {
        addHandler(handler, UpdateWaiterCommand.TYPE);
    }

    public void addDeletehandler(DeleteWaiterCommandHandler handler) {
        addHandler(handler, DeleteWaiterCommand.TYPE);
    }
}
