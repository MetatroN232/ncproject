package cafe.client.widgets;

import cafe.client.event.ContextClickHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

public class ContextMenu<T> extends Composite {
    interface ContextMenuUiBinder extends UiBinder<HTMLPanel, ContextMenu> {
    }

    @UiField
    VerticalPanel listItems;

    @UiField
    PopupPanel panel;

    private T currentObject;

    private static ContextMenuUiBinder ourUiBinder = GWT.create(ContextMenuUiBinder.class);

    public ContextMenu() {
        initWidget(ourUiBinder.createAndBindUi(this));

    }

    public void addButton(String title, ContextClickHandler<T> click) {
        Button button = new Button(title);
        button.setWidth("100%");
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                click.onClick(currentObject);
                hide();
            }
        });

        listItems.add(button);
    }

    public void show(T object) {
        this.currentObject = object;
        panel.show();
    }

    public void hide() {
        panel.hide();
    }

    public void setPosition(int x, int y) {
        panel.setPopupPosition(x, y);
    }
}