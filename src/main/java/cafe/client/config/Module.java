package cafe.client.config;

import cafe.client.controller.*;
import cafe.client.model.DisposalListModel;
import cafe.client.model.MenuListModel;
import cafe.client.model.OrderListModel;
import cafe.client.model.WaiterListModel;
import cafe.client.view.*;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;

public class Module extends AbstractGinModule{
    @Override
    protected void configure() {
        bind(LifeCycle.class).in(Singleton.class);

        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);

        bind(CafeView.class).in(Singleton.class);

        bind(MenuListController.class).in(Singleton.class);
        bind(MenuListModel.class).in(Singleton.class);
        bind(MenuListView.class).in(Singleton.class);

        bind(OrderListController.class).in(Singleton.class);
        bind(OrderListModel.class).in(Singleton.class);
        bind(OrderListView.class).in(Singleton.class);

        bind(WaiterListController.class).in(Singleton.class);
        bind(WaiterListModel.class).in(Singleton.class);
        bind(WaiterListView.class).in(Singleton.class);

        bind(DisposalListController.class).in(Singleton.class);
        bind(DisposalListModel.class).in(Singleton.class);
        bind(DisposalListView.class).in(Singleton.class);

    }
}
