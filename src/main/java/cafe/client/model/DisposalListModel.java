package cafe.client.model;

import cafe.client.event.ChangeDisposalsEvent;
import cafe.client.event.ChangeDisposalsEventHandler;
import cafe.shared.dto.DisposalDto;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.ArrayList;
import java.util.List;

public class DisposalListModel {

    private final EventBus eventBus;

    private List<DisposalDto> disposals;

    public DisposalListModel() {
        this.eventBus = new SimpleEventBus();
        disposals = new ArrayList<>();
    }

    public List<DisposalDto> getDisposals() {
        return disposals;
    }

    public void setDisposals(List<DisposalDto> disposals) {
        this.disposals = disposals;
        eventBus.fireEvent(new ChangeDisposalsEvent());
    }

    public void addChangeDisposalsHandler(ChangeDisposalsEventHandler handler) {
        eventBus.addHandler(ChangeDisposalsEvent.TYPE, handler);
    }
}
