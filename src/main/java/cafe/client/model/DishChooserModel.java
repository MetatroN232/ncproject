package cafe.client.model;

import cafe.client.event.ChangeCategoriesEvent;
import cafe.client.event.ChangeCategoriesEventHandler;
import cafe.client.event.ChangeDishesEvent;
import cafe.client.event.ChangeDishesEventHandler;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.ArrayList;
import java.util.List;

public class DishChooserModel {

    private List<CategoryDto> categories;
    private List<DishDto> dishes;

    private final EventBus eventBus;

    public DishChooserModel() {
        categories = new ArrayList<>();
        dishes = new ArrayList<>();
        eventBus = new SimpleEventBus();
    }

    public List<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
        eventBus.fireEvent(new ChangeCategoriesEvent());
    }

    public List<DishDto> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishDto> dishes) {
        this.dishes = dishes;
        eventBus.fireEvent(new ChangeDishesEvent());
    }

    public void addChangeCategoriesHandler(ChangeCategoriesEventHandler handler) {
        eventBus.addHandler(ChangeCategoriesEvent.TYPE, handler);
    }

    public void addDishesHandler(ChangeDishesEventHandler handler) {
        eventBus.addHandler(ChangeDishesEvent.TYPE, handler);
    }
}
