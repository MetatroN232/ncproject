package cafe.client.model;

import cafe.client.event.ChangeWaiterList;
import cafe.client.event.ChangeWaiterListHandler;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.List;

public class WaiterListModel {
    private final EventBus eventBus;

    private List<WaiterDto> waiterList;

    public WaiterListModel() {
        this.eventBus = new SimpleEventBus();
    }

    public List<WaiterDto> getWaiterList() {
        return waiterList;
    }

    public void setWaiterList(List<WaiterDto> waiterList) {
        this.waiterList = waiterList;
        eventBus.fireEvent(new ChangeWaiterList());
    }

    public void addChangeWaiterListHandler(ChangeWaiterListHandler handler) {
        eventBus.addHandler(ChangeWaiterList.TYPE, handler);
    }

}
