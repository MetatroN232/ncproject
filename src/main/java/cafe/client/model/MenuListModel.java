package cafe.client.model;

import cafe.client.event.*;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.ArrayList;
import java.util.List;

public class MenuListModel {
    private List<CategoryDto> categories = new ArrayList<>();
    private List<DishDto> dishes = new ArrayList<>();
    //private List<DishHasDisposalDto> prices = new ArrayList<>();

    private final EventBus eventBus;

    public MenuListModel() {
        this.eventBus = new SimpleEventBus();
    }

    public void addHandler(ChangeCategoriesEventHandler eventHandler) {
        eventBus.addHandler(ChangeCategoriesEvent.TYPE, eventHandler);
    }

    public void addHandler(ChangeDishesEventHandler eventHandler) {
        eventBus.addHandler(ChangeDishesEvent.TYPE, eventHandler);
    }

    public void addHandler(RefreshMenuListEventHandler eventHandler) {
        eventBus.addHandler(RefreshMenuListEvent.TYPE, eventHandler);
    }

    public List<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
        eventBus.fireEvent(new ChangeCategoriesEvent());
    }

    public List<DishDto> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishDto> dishes) {
        this.dishes = dishes;
        eventBus.fireEvent(new ChangeDishesEvent());
    }

    public void refresh() {
        eventBus.fireEvent(new RefreshMenuListEvent());
    }
}
