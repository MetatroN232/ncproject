package cafe.client.model;

import cafe.client.event.ChangeOrdersEvent;
import cafe.client.event.ChangeWaiterList;
import cafe.client.event.ChangeWaiterListHandler;
import cafe.shared.dto.OrderDto;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.List;

public class OrderPreviewModel {
    private final EventBus eventBus;

    private OrderDto currentOrder;
    private List<WaiterDto> waiterForChoose;

    public OrderPreviewModel() {
        eventBus = new SimpleEventBus();
    }

    public OrderDto getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(OrderDto currentOrder) {
        this.currentOrder = currentOrder;
        eventBus.fireEvent(new ChangeOrdersEvent());
    }

    public List<WaiterDto> getWaiterForChoose() {
        return waiterForChoose;
    }

    public void setWaiterForChoose(List<WaiterDto> waiterForChoose) {
        this.waiterForChoose = waiterForChoose;
        eventBus.fireEvent(new ChangeWaiterList());
    }

    public void addChangeWaitersHandler(ChangeWaiterListHandler handler) {
        eventBus.addHandler(ChangeWaiterList.TYPE, handler);
    }
}
