package cafe.client.model;

import cafe.client.event.ChangeOrdersEvent;
import cafe.client.event.ChangeOrdersEventHandler;
import cafe.client.event.RefreshOrdersEvent;
import cafe.client.event.RefreshOrdersEventHandler;
import cafe.shared.dto.OrderDetail;
import cafe.shared.dto.OrderDto;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.ArrayList;
import java.util.List;

public class OrderListModel {

    private List<OrderDto> orders = new ArrayList<>();
    private List<OrderDetail> orderDetails = new ArrayList<>();

    private final EventBus eventBus;

    public OrderListModel() {
        eventBus = new SimpleEventBus();
    }

    public List<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDto> orders) {
        this.orders = orders;
        eventBus.fireEvent(new ChangeOrdersEvent());
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void addChangeOrdersHandler(ChangeOrdersEventHandler handler) {
        eventBus.addHandler(ChangeOrdersEvent.TYPE, handler);
    }

    public void refresh() {
        eventBus.fireEvent(new RefreshOrdersEvent());
    }

    public void addRefreshOrdersHandler(RefreshOrdersEventHandler handler) {
        eventBus.addHandler(RefreshOrdersEvent.TYPE, handler);
    }

}
