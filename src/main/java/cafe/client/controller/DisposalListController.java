package cafe.client.controller;

import cafe.client.command.*;
import cafe.client.model.DisposalListModel;
import cafe.client.service.DisposalService;
import cafe.shared.dto.DisposalDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DisposalListController {
    private static final Logger logger = Logger.getLogger(OrderListController.class.getName());
    private final EventBus eventBus;

    private final DisposalListModel disposalListModel;
    private final DisposalService disposalService;

    @Inject
    public DisposalListController(EventBus eventBus, DisposalListModel disposalListModel) {
        this.eventBus = eventBus;
        this.disposalListModel = disposalListModel;

        disposalService = GWT.create(DisposalService.class);
    }

    public void bind() {
        eventBus.addHandler(LoadDisposalsCommand.TYPE, new LoadDisposalsCommandHandler() {
            @Override
            public void onLoadDisposalsCommand(LoadDisposalsCommand event) {
                disposalService.getDisposals(new MethodCallback<List<DisposalDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<DisposalDto> disposalDtos) {
                        disposalListModel.setDisposals(disposalDtos);
                    }
                });
            }
        });

        eventBus.addHandler(CreateDisposalCommand.TYPE, new CreateDisposalCommandHandler() {
            @Override
            public void onCreateDisposalCommand(CreateDisposalCommand event) {
                disposalService.createDisposal(event.getDisposal(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        refreshModel();
                    }
                });
            }
        });

        eventBus.addHandler(UpdateDisposalCommand.TYPE, new UpdateDisposalCommandHandler() {
            @Override
            public void onUpdateDisposalCommand(UpdateDisposalCommand event) {
                disposalService.updateDisposal(event.getDisposal(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        refreshModel();
                    }
                });
            }
        });

        eventBus.addHandler(DeleteDisposalCommand.TYPE, new DeleteDisposalCommandHandler() {
            @Override
            public void onDeleteDisposalCommand(DeleteDisposalCommand event) {
                disposalService.deleteDisposal(event.getDisposal(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        refreshModel();
                    }
                });
            }
        });
    }

    private void refreshModel() {
        eventBus.fireEvent(new LoadDisposalsCommand());
    }
}
