package cafe.client.controller;

import cafe.client.command.*;
import cafe.client.model.MenuListModel;
import cafe.client.service.CategoryService;
import cafe.client.service.DishService;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuListController {
    private static final Logger logger = Logger.getLogger(OrderListController.class.getName());

    private final DishService dishService = GWT.create(DishService.class);
    private final CategoryService categoryService = GWT.create(CategoryService.class);

    private final EventBus eventBus;
    private final MenuListModel menuListModel;

    @Inject
    public MenuListController(EventBus eventBus, MenuListModel menuListModel) {
        this.eventBus = eventBus;
        this.menuListModel = menuListModel;
    }

    public void bind() {
        eventBus.addHandler(LoadCategoryCommand.TYPE, new LoadCategoryCommandHandler() {
            @Override
            public void onLoadCategoryCommand(LoadCategoryCommand event) {
                categoryService.getCategories(new MethodCallback<List<CategoryDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<CategoryDto> categoryDtos) {
                        menuListModel.setCategories(categoryDtos);
                    }
                });
            }
        });

        eventBus.addHandler(LoadDishByCategoryCommand.TYPE, new LoadDishByCategoryCommandHandler() {
            @Override
            public void onLoadDishByCategoryCommand(LoadDishByCategoryCommand event) {
                categoryService.getDishByCategory(event.getCategoryId(), new MethodCallback<List<DishDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<DishDto> dishDtos) {
                        menuListModel.setDishes(dishDtos);
                    }
                });

            }
        });

        eventBus.addHandler(CreateCategoryCommand.TYPE, new CreateCategoryCommandHandler() {
            @Override
            public void onCreateCategoryCommand(CreateCategoryCommand event) {
                categoryService.createCategory(event.getCategory(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        menuListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(UpdateCategoryCommand.TYPE, new UpdateCategoryCommandHandler() {
            @Override
            public void onUpdateCategoryCommand(UpdateCategoryCommand event) {
                categoryService.updateCategory(event.getCategory(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        menuListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(DeleteCategoryCommand.TYPE, new DeleteCategoryCommandHandler() {
            @Override
            public void onDeleteCategoryCommand(DeleteCategoryCommand event) {
                categoryService.deleteCategory(event.getCategory(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        menuListModel.refresh();
                    }
                });
            }
        });


        eventBus.addHandler(FindDishByNameCommand.TYPE, new FindDishByNameCommandHandler() {
            @Override
            public void onFindDishByName(FindDishByNameCommand event) {
                dishService.findDishByName(event.getName(), new MethodCallback<List<DishDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<DishDto> dishDtos) {
                        menuListModel.setDishes(dishDtos);
                    }
                });
            }
        });

        eventBus.addHandler(CreateDishCommand.TYPE, new CreateDishCommandHandler() {
            @Override
            public void onCreateDishCommand(CreateDishCommand event) {
                dishService.createDish(event.getDish(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        menuListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(UpdateDishCommand.TYPE, new UpdateDishCommandHandler() {
            @Override
            public void onUpdateDishCommand(UpdateDishCommand event) {
                dishService.updateDish(event.getDish(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        menuListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(DeleteDishCommand.TYPE, new DeleteDishCommandHandler() {
            @Override
            public void onDeleteDishCommand(DeleteDishCommand event) {
                dishService.deleteDish(event.getDish(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        menuListModel.refresh();
                    }
                });
            }
        });
    }
}
