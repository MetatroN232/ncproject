package cafe.client.controller;

import cafe.client.command.*;
import cafe.client.model.DishChooserModel;
import cafe.client.service.CategoryService;
import cafe.client.service.DishService;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DishChooserController {
    private static final Logger logger = Logger.getLogger(OrderListController.class.getName());
    private final EventBus eventBus;
    private final DishChooserModel dishChooserModel;

    private final DishService dishService = GWT.create(DishService.class);
    private final CategoryService categoryService = GWT.create(CategoryService.class);

    public DishChooserController(EventBus eventBus, DishChooserModel dishChooserModel) {
        this.eventBus = eventBus;
        this.dishChooserModel = dishChooserModel;
    }

    public void bind() {
        eventBus.addHandler(LoadDishesByCategoryAndDateChooserCommand.TYPE,
                new LoadDishesByCategoryAndDateChooserCommandHandler() {
                    @Override
                    public void onLoadDishesByCategoryAndDateChooserCommand(LoadDishesByCategoryAndDateChooserCommand event) {
                        //TODO get dishes with price on date
                        categoryService.getDishesByCategoryWithPricesByDate(
                                event.getCategory().getCategoryId(),
                                event.getDate(),
                                new MethodCallback<List<DishDto>>() {
                                    @Override
                                    public void onFailure(Method method, Throwable throwable) {
                                        logger.log(Level.SEVERE, "error", throwable);
                                    }

                                    @Override
                                    public void onSuccess(Method method, List<DishDto> dishDtos) {
                                        dishChooserModel.setDishes(dishDtos);
                                    }
                                });
                    }
                });

        eventBus.addHandler(LoadDishesByCategoryChooserCommand.TYPE,
                new LoadDishesbyCategoryChooserCommandHandler() {
                    @Override
                    public void onLoadDishesByCategoryChooserCommand(LoadDishesByCategoryChooserCommand event) {
                        categoryService.getDishByCategory(event.getCategory().getCategoryId(),
                                new MethodCallback<List<DishDto>>() {
                                    @Override
                                    public void onFailure(Method method, Throwable throwable) {
                                        logger.log(Level.SEVERE, "error", throwable);
                                    }

                                    @Override
                                    public void onSuccess(Method method, List<DishDto> dishDtos) {
                                        dishChooserModel.setDishes(dishDtos);
                                    }
                                });
                    }
                });

        eventBus.addHandler(LoadCategoriesChooserCommand.TYPE, new LoadCategoriesChooserCommandHandler() {
            @Override
            public void onLoadCategoriesChooserCommand(LoadCategoriesChooserCommand event) {
                categoryService.getCategories(new MethodCallback<List<CategoryDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<CategoryDto> categoryDtos) {
                        dishChooserModel.setCategories(categoryDtos);
                    }
                });
            }
        });
    }
}
