package cafe.client.controller;

import cafe.client.command.*;
import cafe.client.model.OrderListModel;
import cafe.client.service.OrderService;
import cafe.shared.dto.OrderDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderListController {
    private static final Logger logger = Logger.getLogger(OrderListController.class.getName());

    private final EventBus eventBus;
    private final OrderListModel orderListModel;
    private final OrderService orderService;

    @Inject
    public OrderListController(EventBus eventBus, OrderListModel orderListModel) {
        this.eventBus = eventBus;
        this.orderListModel = orderListModel;
        orderService = GWT.create(OrderService.class);
    }

    public void bind() {
        eventBus.addHandler(LoadOrdersCommand.TYPE, new LoadOrdersCommandHandler() {
            @Override
            public void onLoadOrdersCommand(LoadOrdersCommand event) {
                orderService.getOrders(new MethodCallback<List<OrderDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<OrderDto> orderDtos) {
                        orderListModel.setOrders(orderDtos);
                    }
                });
            }
        });

        eventBus.addHandler(CreateOrderCommand.TYPE, new CreateOrderCommandHandler() {
            @Override
            public void onCreateOrderCommand(CreateOrderCommand event) {
                orderService.createOrder(event.getOrder(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        orderListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(UpdateOrderCommand.TYPE, new UpdateOrderCommandHandler() {
            @Override
            public void onUpdateOrderCommand(UpdateOrderCommand event) {
                orderService.updateOrder(event.getOrder(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        orderListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(DeleteOrderCommand.TYPE, new DeleteOrderCommandHandler() {
            @Override
            public void onDeleteOrderCommand(DeleteOrderCommand event) {
                orderService.deleteOrder(event.getOrder(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        orderListModel.refresh();
                    }
                });
            }
        });

        eventBus.addHandler(LoadOrdersByFilterCommand.TYPE, new LoadOrdersByFilterCommandHandler() {
            @Override
            public void onLoadOrdersByFilterCommand(LoadOrdersByFilterCommand event) {
                orderService.getOrdersByFilterAndSort(event.getFilterMap(), "date", new MethodCallback<List<OrderDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<OrderDto> orderDtos) {
                        orderListModel.setOrders(orderDtos);
                    }
                });
            }
        });

        eventBus.addHandler(OpenOrderCheckCommand.TYPE, new OpenOrderCheckCommandHandler() {
            @Override
            public void onOpenOrderCheckCommand(OpenOrderCheckCommand event) {
                String url = "api/orders/reports/check" + event.getOrder().getOrderId();
                Window.open(GWT.getHostPageBaseURL() + url, "_blank", "");
            }
        });

        eventBus.addHandler(ChangeOrderStatusCommand.TYPE, new ChangeOrderStatusCommandHandler() {
            @Override
            public void onChangeOrderStatusCommand(ChangeOrderStatusCommand event) {
                OrderDto order = event.getOrder();

                orderService.setStatus(order.getOrderId(), !order.isActive(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void response) {
                        orderListModel.refresh();
                    }
                });
            }
        });
    }
}
