package cafe.client.controller;

import cafe.client.command.*;
import cafe.client.model.WaiterListModel;
import cafe.client.service.WaiterService;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WaiterListController {
    private static final Logger logger = Logger.getLogger(OrderListController.class.getName());
    private final WaiterService waiterService = GWT.create(WaiterService.class);
    private final EventBus eventBus;

    private final WaiterListModel waiterListModel;

    @Inject
    public WaiterListController(EventBus eventBus, WaiterListModel waiterListModel) {
        this.eventBus = eventBus;
        this.waiterListModel = waiterListModel;
    }

    public void bind() {
        eventBus.addHandler(CreateWaiterCommand.TYPE, new CreateWaiterCommandHandler() {
            @Override
            public void onCreateWaiterCommand(CreateWaiterCommand event) {
                waiterService.createWaiter(event.getWaiter(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {

                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        refresh();
                    }
                });
            }
        });

        eventBus.addHandler(UpdateWaiterCommand.TYPE, new UpdateWaiterCommandHandler() {
            @Override
            public void onUpdateWaiterCommand(UpdateWaiterCommand event) {
                waiterService.updateWaiter(event.getWaiter(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        refresh();
                    }
                });
            }
        });

        eventBus.addHandler(DeleteWaiterCommand.TYPE, new DeleteWaiterCommandHandler() {
            @Override
            public void onDeleteWaiterCommand(DeleteWaiterCommand event) {
                waiterService.deleteWaiter(event.getWaiter(), new MethodCallback<Void>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, Void aVoid) {
                        refresh();
                    }
                });
            }
        });

        eventBus.addHandler(LoadWaitersCommand.TYPE, new LoadWaitersCommandHandler() {
            @Override
            public void onLoadWaitersCommand(LoadWaitersCommand event) {
                waiterService.getWaiters(new MethodCallback<List<WaiterDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<WaiterDto> waiterDtos) {
                        waiterListModel.setWaiterList(waiterDtos);
                    }
                });
            }
        });
    }

    public void refresh() {
        eventBus.fireEvent(new LoadWaitersCommand());
    }
}
