package cafe.client.controller;

import cafe.client.command.LoadWaitersCommand;
import cafe.client.command.LoadWaitersCommandHandler;
import cafe.client.model.OrderPreviewModel;
import cafe.client.service.WaiterService;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderPreviewController {
    private static final Logger logger = Logger.getLogger(OrderListController.class.getName());

    private final EventBus eventBus;

    private final OrderPreviewModel orderPreviewModel;

    private final WaiterService waiterService;

    public OrderPreviewController(EventBus eventBus, OrderPreviewModel orderPreviewModel) {
        this.eventBus = eventBus;
        this.orderPreviewModel = orderPreviewModel;
        waiterService = GWT.create(WaiterService.class);
    }

    public void bind() {
        eventBus.addHandler(LoadWaitersCommand.TYPE, new LoadWaitersCommandHandler() {
            @Override
            public void onLoadWaitersCommand(LoadWaitersCommand event) {
                waiterService.getWaiters(new MethodCallback<List<WaiterDto>>() {
                    @Override
                    public void onFailure(Method method, Throwable throwable) {
                        logger.log(Level.SEVERE, "error", throwable);
                    }

                    @Override
                    public void onSuccess(Method method, List<WaiterDto> waiterDtos) {
                        orderPreviewModel.setWaiterForChoose(waiterDtos);
                    }
                });
            }
        });
    }
}
