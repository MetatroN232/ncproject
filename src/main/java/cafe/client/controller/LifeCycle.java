package cafe.client.controller;

import cafe.client.model.DisposalListModel;
import cafe.client.model.MenuListModel;
import cafe.client.model.OrderListModel;
import cafe.client.model.WaiterListModel;
import cafe.client.view.*;

import javax.inject.Inject;

public class LifeCycle {
    private final MenuListModel menuListModel;
    private final MenuListController menuListController;
    private final MenuListView menuListView;

    private final OrderListController orderListController;
    private final OrderListModel orderListModel;
    private final OrderListView orderListView;

    private final WaiterListModel waiterListModel;
    private final WaiterListController waiterListController;
    private final WaiterListView waiterListView;

    private final DisposalListController disposalListController;
    private final DisposalListModel disposalListModel;
    private final DisposalListView disposalListView;

    private final CafeView cafeView;

    @Inject
    public LifeCycle(MenuListModel menuListModel,
                     MenuListController menuListController,
                     MenuListView menuListView,
                     OrderListController orderListController,
                     OrderListModel orderListModel,
                     OrderListView orderListView,
                     WaiterListModel waiterListModel,
                     WaiterListController waiterListController,
                     WaiterListView waiterListView,
                     DisposalListController disposalListController,
                     DisposalListModel disposalListModel,
                     DisposalListView disposalListView,
                     CafeView cafeView) {
        this.menuListModel = menuListModel;
        this.menuListController = menuListController;
        this.menuListView = menuListView;
        this.orderListController = orderListController;
        this.orderListModel = orderListModel;
        this.orderListView = orderListView;
        this.waiterListModel = waiterListModel;
        this.waiterListController = waiterListController;
        this.waiterListView = waiterListView;
        this.disposalListController = disposalListController;
        this.disposalListModel = disposalListModel;
        this.disposalListView = disposalListView;
        this.cafeView = cafeView;
    }

    public void start() {
        menuListController.bind();
        menuListView.bind();

        orderListController.bind();
        orderListView.bind();

        waiterListController.bind();
        waiterListView.bind();

        disposalListController.bind();
        disposalListView.bind();

        cafeView.bind();
    }
}
