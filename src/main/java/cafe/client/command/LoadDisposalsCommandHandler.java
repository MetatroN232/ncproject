package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadDisposalsCommandHandler extends EventHandler {
    void onLoadDisposalsCommand(LoadDisposalsCommand event);
}
