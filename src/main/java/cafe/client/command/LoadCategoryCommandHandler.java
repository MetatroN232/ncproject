package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadCategoryCommandHandler extends EventHandler {
    void onLoadCategoryCommand(LoadCategoryCommand event);
}
