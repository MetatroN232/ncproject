package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface ChangeOrderStatusCommandHandler extends EventHandler {
    void onChangeOrderStatusCommand(ChangeOrderStatusCommand event);
}
