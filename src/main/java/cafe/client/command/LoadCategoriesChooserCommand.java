package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class LoadCategoriesChooserCommand extends GwtEvent<LoadCategoriesChooserCommandHandler> {
    public static Type<LoadCategoriesChooserCommandHandler> TYPE = new Type<LoadCategoriesChooserCommandHandler>();

    public Type<LoadCategoriesChooserCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadCategoriesChooserCommandHandler handler) {
        handler.onLoadCategoriesChooserCommand(this);
    }
}
