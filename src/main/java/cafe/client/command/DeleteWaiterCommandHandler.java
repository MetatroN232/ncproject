package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteWaiterCommandHandler extends EventHandler {
    void onDeleteWaiterCommand(DeleteWaiterCommand event);
}
