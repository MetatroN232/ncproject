package cafe.client.command;

import cafe.shared.dto.WaiterDto;
import com.google.gwt.event.shared.GwtEvent;

public class CreateWaiterCommand extends GwtEvent<CreateWaiterCommandHandler> {
    public static Type<CreateWaiterCommandHandler> TYPE = new Type<CreateWaiterCommandHandler>();

    private WaiterDto waiter;

    public CreateWaiterCommand(WaiterDto waiter) {
        this.waiter = waiter;
    }

    public WaiterDto getWaiter() {
        return waiter;
    }

    public Type<CreateWaiterCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CreateWaiterCommandHandler handler) {
        handler.onCreateWaiterCommand(this);
    }
}
