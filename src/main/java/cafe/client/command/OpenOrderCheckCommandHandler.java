package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by Arseniy on 22.05.2017.
 */
public interface OpenOrderCheckCommandHandler extends EventHandler {
    void onOpenOrderCheckCommand(OpenOrderCheckCommand event);
}
