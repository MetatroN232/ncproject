package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface CreateCategoryCommandHandler extends EventHandler {
    void onCreateCategoryCommand(CreateCategoryCommand event);
}
