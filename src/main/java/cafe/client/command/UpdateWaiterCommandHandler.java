package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface UpdateWaiterCommandHandler extends EventHandler {
    void onUpdateWaiterCommand(UpdateWaiterCommand event);
}
