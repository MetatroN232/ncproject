package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadDishesByCategoryAndDateChooserCommandHandler extends EventHandler {
    void onLoadDishesByCategoryAndDateChooserCommand(LoadDishesByCategoryAndDateChooserCommand event);
}
