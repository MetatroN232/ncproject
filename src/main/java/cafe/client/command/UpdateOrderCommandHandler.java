package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface UpdateOrderCommandHandler extends EventHandler {
    void onUpdateOrderCommand(UpdateOrderCommand event);
}
