package cafe.client.command;

import cafe.shared.dto.OrderDto;
import com.google.gwt.event.shared.GwtEvent;

public class DeleteOrderCommand extends GwtEvent<DeleteOrderCommandHandler> {
    public static Type<DeleteOrderCommandHandler> TYPE = new Type<DeleteOrderCommandHandler>();

    private OrderDto order;

    public DeleteOrderCommand(OrderDto order) {
        this.order = order;
    }

    public OrderDto getOrder() {
        return order;
    }

    public Type<DeleteOrderCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteOrderCommandHandler handler) {
        handler.onDeleteOrderCommand(this);
    }
}
