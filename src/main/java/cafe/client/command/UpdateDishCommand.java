package cafe.client.command;

import cafe.shared.dto.DishDto;
import com.google.gwt.event.shared.GwtEvent;

public class UpdateDishCommand extends GwtEvent<UpdateDishCommandHandler> {
    public static Type<UpdateDishCommandHandler> TYPE = new Type<UpdateDishCommandHandler>();

    public Type<UpdateDishCommandHandler> getAssociatedType() {
        return TYPE;
    }

    private DishDto dish;

    public UpdateDishCommand(DishDto dish) {
        this.dish = dish;
    }

    public DishDto getDish() {
        return dish;
    }

    protected void dispatch(UpdateDishCommandHandler handler) {
        handler.onUpdateDishCommand(this);
    }
}
