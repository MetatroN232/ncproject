package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteDisposalCommandHandler extends EventHandler {
    void onDeleteDisposalCommand(DeleteDisposalCommand event);
}
