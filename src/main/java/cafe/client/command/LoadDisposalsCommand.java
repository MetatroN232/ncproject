package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class LoadDisposalsCommand extends GwtEvent<LoadDisposalsCommandHandler> {
    public static Type<LoadDisposalsCommandHandler> TYPE = new Type<LoadDisposalsCommandHandler>();

    public Type<LoadDisposalsCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadDisposalsCommandHandler handler) {
        handler.onLoadDisposalsCommand(this);
    }
}
