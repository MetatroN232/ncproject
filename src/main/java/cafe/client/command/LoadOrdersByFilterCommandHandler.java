package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadOrdersByFilterCommandHandler extends EventHandler {
    void onLoadOrdersByFilterCommand(LoadOrdersByFilterCommand event);
}
