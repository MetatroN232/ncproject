package cafe.client.command;

import cafe.shared.dto.OrderDto;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by Arseniy on 22.05.2017.
 */
public class OpenOrderCheckCommand extends GwtEvent<OpenOrderCheckCommandHandler> {
    public static Type<OpenOrderCheckCommandHandler> TYPE = new Type<OpenOrderCheckCommandHandler>();

    private OrderDto order;

    public OpenOrderCheckCommand(OrderDto order) {
        this.order = order;
    }

    public OrderDto getOrder() {
        return order;
    }

    public Type<OpenOrderCheckCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(OpenOrderCheckCommandHandler handler) {
        handler.onOpenOrderCheckCommand(this);
    }
}
