package cafe.client.command;

import cafe.shared.dto.OrderDto;
import com.google.gwt.event.shared.GwtEvent;

public class UpdateOrderCommand extends GwtEvent<UpdateOrderCommandHandler> {
    public static Type<UpdateOrderCommandHandler> TYPE = new Type<UpdateOrderCommandHandler>();

    private OrderDto order;

    public UpdateOrderCommand(OrderDto order) {
        this.order = order;
    }

    public OrderDto getOrder() {
        return order;
    }

    public Type<UpdateOrderCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateOrderCommandHandler handler) {
        handler.onUpdateOrderCommand(this);
    }
}
