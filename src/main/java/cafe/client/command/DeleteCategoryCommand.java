package cafe.client.command;

import cafe.shared.dto.CategoryDto;
import com.google.gwt.event.shared.GwtEvent;

public class DeleteCategoryCommand extends GwtEvent<DeleteCategoryCommandHandler> {
    public static Type<DeleteCategoryCommandHandler> TYPE = new Type<DeleteCategoryCommandHandler>();

    private CategoryDto category;

    public DeleteCategoryCommand(CategoryDto category) {
        this.category = category;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public Type<DeleteCategoryCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteCategoryCommandHandler handler) {
        handler.onDeleteCategoryCommand(this);
    }
}
