package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

import java.util.Map;

public class LoadOrdersByFilterCommand extends GwtEvent<LoadOrdersByFilterCommandHandler> {
    public static Type<LoadOrdersByFilterCommandHandler> TYPE = new Type<LoadOrdersByFilterCommandHandler>();

    private Map<String, Object> filterMap;

    public LoadOrdersByFilterCommand(Map<String, Object> filterMap) {
        this.filterMap = filterMap;
    }

    public Map<String, Object> getFilterMap() {
        return filterMap;
    }

    public Type<LoadOrdersByFilterCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadOrdersByFilterCommandHandler handler) {
        handler.onLoadOrdersByFilterCommand(this);
    }
}
