package cafe.client.command;

import cafe.shared.dto.DisposalDto;
import com.google.gwt.event.shared.GwtEvent;

public class UpdateDisposalCommand extends GwtEvent<UpdateDisposalCommandHandler> {
    public static Type<UpdateDisposalCommandHandler> TYPE = new Type<UpdateDisposalCommandHandler>();

    private DisposalDto disposal;

    public UpdateDisposalCommand(DisposalDto disposal) {
        this.disposal = disposal;
    }

    public DisposalDto getDisposal() {
        return disposal;
    }

    public Type<UpdateDisposalCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateDisposalCommandHandler handler) {
        handler.onUpdateDisposalCommand(this);
    }
}
