package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class LoadDishByCategoryCommand extends GwtEvent<LoadDishByCategoryCommandHandler> {
    public static Type<LoadDishByCategoryCommandHandler> TYPE = new Type<LoadDishByCategoryCommandHandler>();

    private int categoryId;

    public LoadDishByCategoryCommand(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Type<LoadDishByCategoryCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadDishByCategoryCommandHandler handler) {
        handler.onLoadDishByCategoryCommand(this);
    }
}
