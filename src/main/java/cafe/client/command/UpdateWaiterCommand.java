package cafe.client.command;

import cafe.shared.dto.WaiterDto;
import com.google.gwt.event.shared.GwtEvent;

public class UpdateWaiterCommand extends GwtEvent<UpdateWaiterCommandHandler> {
    public static Type<UpdateWaiterCommandHandler> TYPE = new Type<UpdateWaiterCommandHandler>();

    private WaiterDto waiter;

    public UpdateWaiterCommand(WaiterDto waiter) {
        this.waiter = waiter;
    }

    public WaiterDto getWaiter() {
        return waiter;
    }

    public Type<UpdateWaiterCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateWaiterCommandHandler handler) {
        handler.onUpdateWaiterCommand(this);
    }
}
