package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface UpdateDisposalCommandHandler extends EventHandler {
    void onUpdateDisposalCommand(UpdateDisposalCommand event);
}
