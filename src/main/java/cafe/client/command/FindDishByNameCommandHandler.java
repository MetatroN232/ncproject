package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface FindDishByNameCommandHandler extends EventHandler {
    void onFindDishByName(FindDishByNameCommand event);
}
