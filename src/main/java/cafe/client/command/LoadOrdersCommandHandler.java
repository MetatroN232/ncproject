package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadOrdersCommandHandler extends EventHandler {
    void onLoadOrdersCommand(LoadOrdersCommand event);
}
