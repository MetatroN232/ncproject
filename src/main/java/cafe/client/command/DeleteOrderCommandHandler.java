package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteOrderCommandHandler extends EventHandler {
    void onDeleteOrderCommand(DeleteOrderCommand event);
}
