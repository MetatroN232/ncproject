package cafe.client.command;

import cafe.shared.dto.WaiterDto;
import com.google.gwt.event.shared.GwtEvent;

public class DeleteWaiterCommand extends GwtEvent<DeleteWaiterCommandHandler> {
    public static Type<DeleteWaiterCommandHandler> TYPE = new Type<DeleteWaiterCommandHandler>();

    private WaiterDto waiter;

    public DeleteWaiterCommand(WaiterDto waiter) {
        this.waiter = waiter;
    }

    public WaiterDto getWaiter() {
        return waiter;
    }

    public Type<DeleteWaiterCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteWaiterCommandHandler handler) {
        handler.onDeleteWaiterCommand(this);
    }
}
