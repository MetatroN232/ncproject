package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadDishesbyCategoryChooserCommandHandler extends EventHandler {
    void onLoadDishesByCategoryChooserCommand(LoadDishesByCategoryChooserCommand event);
}
