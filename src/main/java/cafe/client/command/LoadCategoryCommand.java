package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class LoadCategoryCommand extends GwtEvent<LoadCategoryCommandHandler> {
    public static Type<LoadCategoryCommandHandler> TYPE = new Type<LoadCategoryCommandHandler>();

    public Type<LoadCategoryCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadCategoryCommandHandler handler) {
        handler.onLoadCategoryCommand(this);
    }
}
