package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface CreateDisposalCommandHandler extends EventHandler {
    void onCreateDisposalCommand(CreateDisposalCommand event);
}
