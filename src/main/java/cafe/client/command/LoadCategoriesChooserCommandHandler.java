package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadCategoriesChooserCommandHandler extends EventHandler {
    void onLoadCategoriesChooserCommand(LoadCategoriesChooserCommand event);
}
