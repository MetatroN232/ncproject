package cafe.client.command;

import cafe.shared.dto.CategoryDto;
import com.google.gwt.event.shared.GwtEvent;

public class CreateCategoryCommand extends GwtEvent<CreateCategoryCommandHandler> {
    public static Type<CreateCategoryCommandHandler> TYPE = new Type<CreateCategoryCommandHandler>();

    private CategoryDto categroy;

    public CreateCategoryCommand(CategoryDto categroy) {
        this.categroy = categroy;
    }

    public CategoryDto getCategory() {
        return categroy;
    }

    public Type<CreateCategoryCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CreateCategoryCommandHandler handler) {
        handler.onCreateCategoryCommand(this);
    }
}
