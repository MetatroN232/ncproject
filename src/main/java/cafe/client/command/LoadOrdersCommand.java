package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class LoadOrdersCommand extends GwtEvent<LoadOrdersCommandHandler> {
    public static Type<LoadOrdersCommandHandler> TYPE = new Type<LoadOrdersCommandHandler>();

    public Type<LoadOrdersCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadOrdersCommandHandler handler) {
        handler.onLoadOrdersCommand(this);
    }
}
