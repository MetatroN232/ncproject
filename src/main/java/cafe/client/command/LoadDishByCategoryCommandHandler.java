package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadDishByCategoryCommandHandler extends EventHandler {
    void onLoadDishByCategoryCommand(LoadDishByCategoryCommand event);
}
