package cafe.client.command;

import cafe.shared.dto.CategoryDto;
import com.google.gwt.event.shared.GwtEvent;

public class LoadDishesByCategoryChooserCommand extends GwtEvent<LoadDishesbyCategoryChooserCommandHandler> {
    public static Type<LoadDishesbyCategoryChooserCommandHandler> TYPE = new Type<LoadDishesbyCategoryChooserCommandHandler>();

    private CategoryDto category;

    public LoadDishesByCategoryChooserCommand(CategoryDto category) {
        this.category = category;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public Type<LoadDishesbyCategoryChooserCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadDishesbyCategoryChooserCommandHandler handler) {
        handler.onLoadDishesByCategoryChooserCommand(this);
    }
}
