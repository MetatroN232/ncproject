package cafe.client.command;

import cafe.shared.dto.CategoryDto;
import com.google.gwt.event.shared.GwtEvent;

public class UpdateCategoryCommand extends GwtEvent<UpdateCategoryCommandHandler> {
    public static Type<UpdateCategoryCommandHandler> TYPE = new Type<UpdateCategoryCommandHandler>();

    private CategoryDto category;

    public UpdateCategoryCommand(CategoryDto category) {
        this.category = category;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public Type<UpdateCategoryCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(UpdateCategoryCommandHandler handler) {
        handler.onUpdateCategoryCommand(this);
    }
}
