package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class FindDishByNameCommand extends GwtEvent<FindDishByNameCommandHandler> {
    public static Type<FindDishByNameCommandHandler> TYPE = new Type<FindDishByNameCommandHandler>();

    private String name;

    public FindDishByNameCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Type<FindDishByNameCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(FindDishByNameCommandHandler handler) {
        handler.onFindDishByName(this);
    }
}
