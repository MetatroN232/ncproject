package cafe.client.command;

import cafe.shared.dto.DisposalDto;
import com.google.gwt.event.shared.GwtEvent;

public class CreateDisposalCommand extends GwtEvent<CreateDisposalCommandHandler> {
    public static Type<CreateDisposalCommandHandler> TYPE = new Type<CreateDisposalCommandHandler>();

    private DisposalDto disposal;

    public CreateDisposalCommand(DisposalDto disposal) {
        this.disposal = disposal;
    }

    public DisposalDto getDisposal() {
        return disposal;
    }

    public Type<CreateDisposalCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CreateDisposalCommandHandler handler) {
        handler.onCreateDisposalCommand(this);
    }
}
