package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface UpdateCategoryCommandHandler extends EventHandler {
    void onUpdateCategoryCommand(UpdateCategoryCommand event);
}
