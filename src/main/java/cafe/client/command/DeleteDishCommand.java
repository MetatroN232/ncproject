package cafe.client.command;

import cafe.shared.dto.DishDto;
import com.google.gwt.event.shared.GwtEvent;

public class DeleteDishCommand extends GwtEvent<DeleteDishCommandHandler> {
    public static Type<DeleteDishCommandHandler> TYPE = new Type<DeleteDishCommandHandler>();

    private DishDto dish;

    public DeleteDishCommand(DishDto dish) {
        this.dish = dish;
    }

    public DishDto getDish() {
        return dish;
    }

    public Type<DeleteDishCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteDishCommandHandler handler) {
        handler.onDeleteDishCommand(this);
    }
}
