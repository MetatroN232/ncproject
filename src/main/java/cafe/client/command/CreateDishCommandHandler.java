package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface CreateDishCommandHandler extends EventHandler {
    void onCreateDishCommand(CreateDishCommand event);
}
