package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface UpdateDishCommandHandler extends EventHandler {
    void onUpdateDishCommand(UpdateDishCommand event);
}
