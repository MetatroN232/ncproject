package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface LoadWaitersCommandHandler extends EventHandler {
    void onLoadWaitersCommand(LoadWaitersCommand event);
}
