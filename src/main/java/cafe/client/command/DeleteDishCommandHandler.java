package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteDishCommandHandler extends EventHandler {
    void onDeleteDishCommand(DeleteDishCommand event);
}
