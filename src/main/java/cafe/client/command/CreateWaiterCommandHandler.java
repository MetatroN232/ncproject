package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface CreateWaiterCommandHandler extends EventHandler {
    void onCreateWaiterCommand(CreateWaiterCommand event);
}
