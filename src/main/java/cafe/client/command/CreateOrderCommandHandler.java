package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface CreateOrderCommandHandler extends EventHandler {
    void onCreateOrderCommand(CreateOrderCommand event);
}
