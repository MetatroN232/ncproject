package cafe.client.command;

import cafe.shared.dto.DisposalDto;
import com.google.gwt.event.shared.GwtEvent;

public class DeleteDisposalCommand extends GwtEvent<DeleteDisposalCommandHandler> {
    public static Type<DeleteDisposalCommandHandler> TYPE = new Type<DeleteDisposalCommandHandler>();

    private DisposalDto disposal;

    public DeleteDisposalCommand(DisposalDto disposal) {
        this.disposal = disposal;
    }

    public DisposalDto getDisposal() {
        return disposal;
    }

    public Type<DeleteDisposalCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(DeleteDisposalCommandHandler handler) {
        handler.onDeleteDisposalCommand(this);
    }
}
