package cafe.client.command;

import cafe.shared.dto.OrderDto;
import com.google.gwt.event.shared.GwtEvent;

public class CreateOrderCommand extends GwtEvent<CreateOrderCommandHandler> {
    public static Type<CreateOrderCommandHandler> TYPE = new Type<CreateOrderCommandHandler>();

    private OrderDto order;

    public CreateOrderCommand(OrderDto order) {
        this.order = order;
    }

    public OrderDto getOrder() {
        return order;
    }

    public Type<CreateOrderCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(CreateOrderCommandHandler handler) {
        handler.onCreateOrderCommand(this);
    }
}
