package cafe.client.command;

import cafe.shared.dto.OrderDto;
import com.google.gwt.event.shared.GwtEvent;

public class ChangeOrderStatusCommand extends GwtEvent<ChangeOrderStatusCommandHandler> {
    public static Type<ChangeOrderStatusCommandHandler> TYPE = new Type<ChangeOrderStatusCommandHandler>();

    private OrderDto order;

    public ChangeOrderStatusCommand(OrderDto order) {
        this.order = order;
    }

    public OrderDto getOrder() {
        return order;
    }

    public Type<ChangeOrderStatusCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(ChangeOrderStatusCommandHandler handler) {
        handler.onChangeOrderStatusCommand(this);
    }
}
