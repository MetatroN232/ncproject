package cafe.client.command;

import cafe.shared.dto.DishDto;
import com.google.gwt.event.shared.GwtEvent;

public class CreateDishCommand extends GwtEvent<CreateDishCommandHandler> {
    public static Type<CreateDishCommandHandler> TYPE = new Type<CreateDishCommandHandler>();

    public Type<CreateDishCommandHandler> getAssociatedType() {
        return TYPE;
    }

    private DishDto dish;

    public CreateDishCommand(DishDto dish) {
        this.dish = dish;
    }

    public DishDto getDish() {
        return dish;
    }

    protected void dispatch(CreateDishCommandHandler handler) {
        handler.onCreateDishCommand(this);
    }
}
