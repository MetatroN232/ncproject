package cafe.client.command;

import com.google.gwt.event.shared.GwtEvent;

public class LoadWaitersCommand extends GwtEvent<LoadWaitersCommandHandler> {
    public static Type<LoadWaitersCommandHandler> TYPE = new Type<LoadWaitersCommandHandler>();

    public Type<LoadWaitersCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadWaitersCommandHandler handler) {
        handler.onLoadWaitersCommand(this);
    }
}
