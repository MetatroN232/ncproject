package cafe.client.command;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteCategoryCommandHandler extends EventHandler {
    void onDeleteCategoryCommand(DeleteCategoryCommand event);
}
