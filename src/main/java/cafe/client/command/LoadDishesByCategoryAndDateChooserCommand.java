package cafe.client.command;

import cafe.shared.dto.CategoryDto;
import com.google.gwt.event.shared.GwtEvent;

public class LoadDishesByCategoryAndDateChooserCommand extends GwtEvent<LoadDishesByCategoryAndDateChooserCommandHandler> {
    public static Type<LoadDishesByCategoryAndDateChooserCommandHandler> TYPE = new Type<LoadDishesByCategoryAndDateChooserCommandHandler>();

    private CategoryDto category;
    private long date;

    public LoadDishesByCategoryAndDateChooserCommand(CategoryDto category, long date) {
        this.category = category;
        this.date = date;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public long getDate() {
        return date;
    }

    public Type<LoadDishesByCategoryAndDateChooserCommandHandler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(LoadDishesByCategoryAndDateChooserCommandHandler handler) {
        handler.onLoadDishesByCategoryAndDateChooserCommand(this);
    }
}
