package cafe.client.view;

import cafe.client.command.*;
import cafe.client.event.ChangeDisposalsEvent;
import cafe.client.event.ChangeDisposalsEventHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.client.model.DisposalListModel;
import cafe.client.widgets.DisposalDetailTable;
import cafe.client.widgets.DisposalTable;
import cafe.client.widgets.IconHtml;
import cafe.shared.dto.DisposalDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import javax.inject.Inject;

public class DisposalListView extends Composite {
    interface DisposalListViewUiBinder extends UiBinder<HTMLPanel, DisposalListView> {
    }

    private static DisposalListViewUiBinder ourUiBinder = GWT.create(DisposalListViewUiBinder.class);

    private final EventBus eventBus;

    private final DisposalListModel disposalListModel;
    @UiField
    Button addDisposalButton;
    @UiField
    DisposalTable disposalTable;
    @UiField
    DisposalDetailTable disposalDetailTable;
    @UiField
    Button refreshButton;

    private final DisposalPreview addDisposalDialog;
    private final SingleSelectionModel<DisposalDto> singleSelectionModel;

    @Inject
    public DisposalListView(EventBus eventBus, DisposalListModel disposalListModel) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.disposalListModel = disposalListModel;
        this.eventBus = eventBus;

        addDisposalDialog = new DisposalPreview();
        singleSelectionModel = new SingleSelectionModel<>();

        disposalTable.setSelectionModel(singleSelectionModel);
        addDisposalButton.setHTML(IconHtml.PLUS);
        refreshButton.setHTML(IconHtml.REFRESH);
    }

    public void bind() {
        addDisposalDialog.bind();
        disposalTable.bind();
        addDisposalButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addDisposalDialog.show();
            }
        });

        addDisposalDialog.addSubmitHandler(new SubmitEventHandler<DisposalDto>() {
            @Override
            public void onSubmit(DisposalDto object) {
                eventBus.fireEvent(new CreateDisposalCommand(object));
            }
        });

        disposalTable.addDeleteDisposalHandler(new DeleteDisposalCommandHandler() {
            @Override
            public void onDeleteDisposalCommand(DeleteDisposalCommand event) {
                eventBus.fireEvent(new DeleteDisposalCommand(event.getDisposal()));
            }
        });

        disposalTable.addSubmitUpdateHandler(new SubmitEventHandler<DisposalDto>() {
            @Override
            public void onSubmit(DisposalDto object) {
                eventBus.fireEvent(new UpdateDisposalCommand(object));
            }
        });

        singleSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                DisposalDto selectedObject = singleSelectionModel.getSelectedObject();
                disposalDetailTable.setRowData(selectedObject.getDisposalDetails());
            }
        });

        disposalListModel.addChangeDisposalsHandler(new ChangeDisposalsEventHandler() {
            @Override
            public void onChangeDisposals(ChangeDisposalsEvent event) {
                updateDisposalTable();
            }
        });

        refreshButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                refresh();
            }
        });

        refresh();
    }

    private void updateDisposalTable() {
        disposalTable.setRowData(disposalListModel.getDisposals());
    }

    private void refresh() {
        eventBus.fireEvent(new LoadDisposalsCommand());
    }

}