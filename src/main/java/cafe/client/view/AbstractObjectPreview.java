package cafe.client.view;

import cafe.client.Console;
import cafe.client.event.SubmitEventHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalFooter;

public abstract class AbstractObjectPreview<T> extends Composite {

    @UiField
    Modal modalPanel;
    @UiField
    ModalFooter footerPanel;
    @UiField
    Button submitButton;
    @UiField
    Button cancelButton;

    protected T currentItem;

    public void show() {
        currentItem = null;
        setAttributes(null);
        modalPanel.show();
    }

    public void show(T object) {
        currentItem = object;
        setAttributes(object);
        modalPanel.show();
    }

    public void hide() {
        modalPanel.hide();
    }

    public void addSubmitHandler(SubmitEventHandler<T> handler) {
        submitButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (checkFields()) {
                    updateCurrentItemValue();
                    handler.onSubmit(currentItem);
                    hide();
                } else {
                    Console.write("check fields error");
                }
            }
        });
    }

    protected boolean checkFields() {
        return true;
    }

    public void setTitle(String title) {
        modalPanel.setTitle(title);
    }

    protected abstract void setAttributes(T object);

    public void bind() {
        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });
    }

    protected abstract void updateCurrentItemValue();
}
