package cafe.client.view;

import cafe.client.command.*;
import cafe.client.event.ChangeWaiterList;
import cafe.client.event.ChangeWaiterListHandler;
import cafe.client.event.SubmitEventHandler;
import cafe.client.model.WaiterListModel;
import cafe.client.widgets.WaiterTable;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

import javax.inject.Inject;

import static cafe.client.widgets.IconHtml.PLUS;
import static cafe.client.widgets.IconHtml.REFRESH;

public class WaiterListView extends Composite {


    interface WaiterListViewUiBinder extends UiBinder<HTMLPanel, WaiterListView> {
    }

    private static WaiterListViewUiBinder ourUiBinder = GWT.create(WaiterListViewUiBinder.class);

    private final EventBus eventBus;

    private final WaiterListModel waiterListModel;
    @UiField
    WaiterTable waiterTable;
    @UiField
    Button addWaiterButton;
    @UiField
    Button refreshButton;

    private final WaiterPreview addWaiterDialog;

    @Inject
    public WaiterListView(EventBus eventBus, WaiterListModel waiterListModel) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.eventBus = eventBus;
        this.waiterListModel = waiterListModel;

        addWaiterDialog = new WaiterPreview();

        refreshButton.setHTML(REFRESH);
        addWaiterButton.setHTML(PLUS);

    }

    public void bind() {
        waiterTable.bind();
        addWaiterDialog.bind();

        addWaiterButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addWaiterDialog.show();
            }
        });

        addWaiterDialog.addSubmitHandler(new SubmitEventHandler<WaiterDto>() {
            @Override
            public void onSubmit(WaiterDto object) {
                eventBus.fireEvent(new CreateWaiterCommand(object));
            }
        });

        waiterTable.addUpdateHandler(new UpdateWaiterCommandHandler() {
            @Override
            public void onUpdateWaiterCommand(UpdateWaiterCommand event) {
                eventBus.fireEvent(event);
            }
        });

        waiterTable.addDeletehandler(new DeleteWaiterCommandHandler() {
            @Override
            public void onDeleteWaiterCommand(DeleteWaiterCommand event) {
                eventBus.fireEvent(event);
            }
        });

        waiterListModel.addChangeWaiterListHandler(new ChangeWaiterListHandler() {
            @Override
            public void onChangeWaiterList(ChangeWaiterList event) {
                waiterTable.setRowData(waiterListModel.getWaiterList());
            }
        });

        refreshButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                refresh();
            }
        });

        refresh();

    }

    public void refresh() {
        eventBus.fireEvent(new LoadWaitersCommand());
    }
}