package cafe.client.view;

import cafe.client.command.*;
import cafe.client.event.*;
import cafe.client.model.OrderListModel;
import cafe.client.widgets.OrderDetailTable;
import cafe.client.widgets.OrderTable;
import cafe.shared.dto.OrderDto;
import cafe.shared.dto.OrderStatus;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.ValueListBox;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cafe.client.widgets.IconHtml.PLUS;
import static cafe.client.widgets.IconHtml.REFRESH;

public class OrderListView extends Composite {
    interface OrderListUiBinder extends UiBinder<HTMLPanel, OrderListView> {
    }

    private static OrderListUiBinder ourUiBinder = GWT.create(OrderListUiBinder.class);

    @UiField
    HorizontalPanel orderToolbar;
    @UiField
    HorizontalPanel orderDetailToolBar;
    @UiField
    Button refreshButton;
    @UiField
    Button addOrderButton;
    @UiField
    OrderTable orderTable;
    @UiField
    OrderDetailTable orderDetailTable;
    @UiField(provided = true)
    ValueListBox<OrderStatus> statusList;
    @UiField
    TextBox numTableTextBox;

    private final String ORDER_STATUS_ATTR = "active";
    private final String TABLE_NUMBER_ATTR = "numTable";

    private final TestView testView = new TestView();

    private final EventBus eventBus;
    private final OrderListModel model;

    private final SingleSelectionModel<OrderDto> selectionModel;

    private final OrderPreview addOrderDialog;

    private final Map<String, Object> filterMap;

    @Inject
    public OrderListView(EventBus eventBus, OrderListModel model) {

        statusList = new ValueListBox<OrderStatus>(new AbstractRenderer<OrderStatus>() {
            @Override
            public String render(OrderStatus object) {
                if (object != null) {
                    return object.getText();
                } else {
                    return null;
                }
            }
        });

        initWidget(ourUiBinder.createAndBindUi(this));

        this.eventBus = eventBus;
        this.model = model;

        selectionModel = new SingleSelectionModel<>();
        addOrderDialog = new OrderPreview();
        filterMap = new HashMap<>();

        orderTable.setSelectionModel(selectionModel);
        init();
    }

    private void init() {
        refreshButton.setHTML(REFRESH);
        addOrderButton.setHTML(PLUS);

        numTableTextBox.setPlaceholder("Номер столика");

        List<OrderStatus> acceptableStatus = new ArrayList<>();
        acceptableStatus.add(OrderStatus.ALL);
        acceptableStatus.add(OrderStatus.OPEN);
        acceptableStatus.add(OrderStatus.CLOSE);
        statusList.setValue(acceptableStatus.get(0));
        statusList.setAcceptableValues(acceptableStatus);
    }

    public void bind() {
        bindModels();
        addOrderDialog.bind();
        orderTable.bind();

        refreshButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                refresh();
            }
        });

        addOrderButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addOrderDialog.show();
            }
        });

        addOrderDialog.addSubmitHandler(new SubmitEventHandler<OrderDto>() {
            @Override
            public void onSubmit(OrderDto object) {
                eventBus.fireEvent(new CreateOrderCommand(object));
            }
        });

        orderTable.addDeleteHandler(new DeleteOrderCommandHandler() {
            @Override
            public void onDeleteOrderCommand(DeleteOrderCommand event) {
                eventBus.fireEvent(event);
                if (orderDetailTable.getVisibleItems() != null &&
                        orderDetailTable.getVisibleItems().get(0).getOrderId() == event.getOrder().getOrderId()) {
                    orderDetailTable.setVisibleRangeAndClearData(orderDetailTable.getVisibleRange(), true);
                }
            }
        });

        orderTable.addSubmitUpdateHandler(new SubmitEventHandler<OrderDto>() {
            @Override
            public void onSubmit(OrderDto order) {
                eventBus.fireEvent(new UpdateOrderCommand(order));
            }
        });

        orderTable.addOpenCheckHandler(new OpenOrderCheckCommandHandler() {
            @Override
            public void onOpenOrderCheckCommand(OpenOrderCheckCommand event) {
                eventBus.fireEvent(event);
            }
        });

        orderTable.addChangeStatusOrderHandler(new ChangeOrderStatusCommandHandler() {
            @Override
            public void onChangeOrderStatusCommand(ChangeOrderStatusCommand event) {
                eventBus.fireEvent(event);
            }
        });

        statusList.addValueChangeHandler(new ValueChangeHandler<OrderStatus>() {
            @Override
            public void onValueChange(ValueChangeEvent<OrderStatus> event) {
                if (OrderStatus.ALL != statusList.getValue()) {
                    filterMap.put(ORDER_STATUS_ATTR, statusList.getValue().getStatus());
                } else {
                    filterMap.remove(ORDER_STATUS_ATTR);
                }
                refresh();
            }
        });

        numTableTextBox.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                if (numTableTextBox.getValue() != null && !numTableTextBox.getValue().equals("")) {
                    filterMap.put(TABLE_NUMBER_ATTR, numTableTextBox.getValue());
                } else {
                    filterMap.remove(TABLE_NUMBER_ATTR);
                }
                refresh();
            }
        });

        refresh();
    }

    private void bindModels() {
        model.addChangeOrdersHandler(new ChangeOrdersEventHandler() {
            @Override
            public void onChangeOrders(ChangeOrdersEvent event) {
                updateOrderTable();
            }
        });

        model.addRefreshOrdersHandler(new RefreshOrdersEventHandler() {
            @Override
            public void onRefreshOrders(RefreshOrdersEvent event) {
                refresh();
            }
        });

        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                OrderDto orderDto = selectionModel.getSelectedObject();
                orderDetailTable.setRowData(orderDto.getOrderDetails());
            }
        });
    }

    private void updateOrderTable() {
        orderTable.setRowData(model.getOrders());
    }

    private void refresh() {
        eventBus.fireEvent(new LoadOrdersByFilterCommand(filterMap));
        //eventBus.fireEvent(new LoadOrdersCommand());
    }


}