package cafe.client.view;

import cafe.client.model.DisposalListModel;
import cafe.client.model.MenuListModel;
import cafe.client.model.OrderListModel;
import cafe.client.model.WaiterListModel;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;

import javax.inject.Inject;

public class CafeView {

    private final  EventBus eventBus;

    private final MenuListModel menuListModel;
    private final OrderListModel orderListModel;
    private final WaiterListModel waiterListModel;
    private final DisposalListModel disposalListModel;

    private final MenuListView menuListView;
    private final OrderListView orderListView;
    private final WaiterListView waiterListView;
    private final DisposalListView disposalListView;
    private final ReportsView reportsView;

    @Inject
    public CafeView(EventBus eventBus,
                    MenuListModel menuListModel,
                    OrderListModel orderListModel,
                    WaiterListModel waiterListModel,
                    DisposalListModel disposalListModel,
                    MenuListView menuListView,
                    OrderListView orderListView,
                    WaiterListView waiterListView,
                    DisposalListView disposalListView,
                    ReportsView reportsView) {
        this.eventBus = eventBus;
        this.menuListModel = menuListModel;
        this.orderListModel = orderListModel;
        this.waiterListModel = waiterListModel;
        this.disposalListModel = disposalListModel;
        this.menuListView = menuListView;
        this.orderListView = orderListView;
        this.waiterListView = waiterListView;
        this.disposalListView = disposalListView;
        this.reportsView = reportsView;


        TabLayoutPanel tab = new TabLayoutPanel(1.5, Style.Unit.EM);
        tab.add(orderListView, "Заказы");
        tab.add(menuListView, "Список блюд");
        tab.add(waiterListView, "Официанты");
        tab.add(disposalListView, "Распоряжения");
        tab.add(reportsView, "Отчеты");

        tab.setHeight("100%");

        RootPanel.get().add(tab);
    }

    public void bind() {
        reportsView.bind();
    }
}
