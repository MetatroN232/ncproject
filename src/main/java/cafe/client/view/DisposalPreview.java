package cafe.client.view;

import cafe.client.widgets.DisposalDetailTable;
import cafe.client.widgets.IntegerInputBox;
import cafe.shared.dto.DisposalDetail;
import cafe.shared.dto.DisposalDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.DateTimePicker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DisposalPreview extends AbstractObjectPreview<DisposalDto> {

    interface DisposalPreviewUiBinder extends UiBinder<HTMLPanel, DisposalPreview> {
    }

    private static DisposalPreviewUiBinder ourUiBinder = GWT.create(DisposalPreviewUiBinder.class);
    @UiField
    IntegerInputBox numDisposalInput;
    @UiField
    DateTimePicker dateTimePicker;
    @UiField
    DisposalDetailTable disposalDetailTable;
    @UiField
    Button addDishButton;
    @UiField
    Button deleteDishButton;

    private DishChooser dishChooser;
    private List<DisposalDetail> disposalDetails;
    private SingleSelectionModel<DisposalDetail> singleSelectionModel;
    private DisposalDetail currentDetail;

    public DisposalPreview() {
        initWidget(ourUiBinder.createAndBindUi(this));
        dishChooser = new DishChooser();
        singleSelectionModel = new SingleSelectionModel<>();

        disposalDetailTable.setSelectionModel(singleSelectionModel);
        addDishButton.setIcon(IconType.PLUS);
        deleteDishButton.setIcon(IconType.MINUS);
    }

    @Override
    protected void setAttributes(DisposalDto object) {
        if (object != null) {
            setTitle("Распоряжение №: " + object.getNumDisposal());
            numDisposalInput.setValue(object.getNumDisposal());
            dateTimePicker.setValue(object.getDate());
            disposalDetails.addAll(object.getDisposalDetails());
        } else {
            setTitle("Новое распоряжение");
            dateTimePicker.setValue(new Date());
            disposalDetailTable.setVisibleRangeAndClearData(disposalDetailTable.getVisibleRange(), true);
        }
    }

    @Override
    protected void updateCurrentItemValue() {
        if (currentItem != null) {
            currentItem.setDate(dateTimePicker.getValue());
            currentItem.setDisposalDetails(disposalDetails);
            currentItem.setNumDisposal(numDisposalInput.getValue());
        } else {
            currentItem = new DisposalDto();
            updateCurrentItemValue();
        }
    }

    private void updateTable() {
        disposalDetailTable.setRowData(disposalDetails);
    }

    @Override
    public void bind() {
        super.bind();
        dishChooser.bind();

        dishChooser.addSubmitHandler(object -> {
            int id = currentItem != null ? currentItem.getDisposalId() : 0;

            for (DisposalDetail dd : disposalDetails) {
                if (dd.getDish().getDishId() == object.getDishId()) {
                    return;
                }
            }
            DisposalDetail disposalDetail = new DisposalDetail(id, object);
            disposalDetails.add(disposalDetail);
            updateTable();
        });

        addDishButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dishChooser.show();
            }
        });

        deleteDishButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (disposalDetails.contains(currentDetail)) {
                    disposalDetails.remove(currentDetail);
                    updateTable();
                }
            }
        });

        singleSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                currentDetail = singleSelectionModel.getSelectedObject();
            }
        });
    }

    @Override
    public void show() {
        disposalDetails = new ArrayList<>();
        super.show();
    }

    @Override
    public void show(DisposalDto object) {
        disposalDetails = new ArrayList<>();
        super.show(object);
        updateTable();
    }

    @Override
    protected boolean checkFields() {
        if (numDisposalInput.getValue() == null) return false;
        if (dateTimePicker.getValue() == null) return false;
        if (disposalDetails.size() == 0) return false;
        for (DisposalDetail dd : disposalDetails) {
            if (dd.getDish().getPrice() < 1) return false;
        }
        return true;
    }
}