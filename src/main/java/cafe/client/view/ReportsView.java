package cafe.client.view;

import cafe.client.widgets.IntegerInputBox;
import cafe.shared.dto.Month;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ValueListBox;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ReportsView extends Composite {
    interface ReportsViewUiBinder extends UiBinder<HTMLPanel, ReportsView> {
    }

    private static ReportsViewUiBinder ourUiBinder = GWT.create(ReportsViewUiBinder.class);
    @UiField(provided = true)
    ValueListBox<Month> monthBox;
    @UiField
    IntegerInputBox yearField;
    @UiField
    Button monthReportButton;
    @UiField
    DateBox date;
    @UiField
    Button dateReportButton;

    private final List<Month> months = Arrays.asList(Month.values());

    public ReportsView() {

        monthBox = new ValueListBox<>(new AbstractRenderer<Month>() {
            @Override
            public String render(Month object) {
                return object.getName();
            }
        });

        initWidget(ourUiBinder.createAndBindUi(this));

        monthBox.setValue(months.get(0));
        monthBox.setAcceptableValues(months);
        date.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd.MM.yyyy")));
        date.setValue(new Date());


    }

    public void bind() {
        monthReportButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (yearField.getValue() != null && yearField.getValue() > 1970) {
                    String url = "api/orders/reports/" + monthBox.getValue().getNumber() + "/" + yearField.getValue();
                    Window.open(GWT.getHostPageBaseURL() + url, "_blank", "");
                }
            }
        });

        dateReportButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (date.getValue() != null) {
                    String url = "api/orders/reports/" + date.getValue().getTime();
                    Window.open(GWT.getHostPageBaseURL() + url, "_blank", "");
                }
            }
        });
    }
}