package cafe.client.view;

import cafe.shared.dto.WaiterDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.DateTimePicker;

import java.util.Date;

public class WaiterPreview extends AbstractObjectPreview<WaiterDto> {

    interface WaiterPreviewUiBinder extends UiBinder<HTMLPanel, WaiterPreview> {
    }

    private static WaiterPreviewUiBinder ourUiBinder = GWT.create(WaiterPreviewUiBinder.class);
    @UiField
    TextBox secondNameInput;
    @UiField
    TextBox firstNameInput;
    @UiField
    TextBox middleNameInput;
    @UiField
    TextBox addressInput;
    @UiField
    DateTimePicker datePicker;

    public WaiterPreview() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    @Override
    protected void setAttributes(WaiterDto waiter) {
        if (waiter != null) {
            secondNameInput.setText(waiter.getFam());
            firstNameInput.setText(waiter.getName());
            middleNameInput.setText(waiter.getOtch());
            addressInput.setText(waiter.getAddress());
            datePicker.setValue(waiter.getBirthday());
        } else {
            secondNameInput.setText("");
            firstNameInput.setText("");
            middleNameInput.setText("");
            addressInput.setText("");
            datePicker.setValue(new Date());
        }
    }

    @Override
    protected void updateCurrentItemValue() {
        if (currentItem != null) {
            currentItem.setName(firstNameInput.getValue());
            currentItem.setFam(secondNameInput.getValue());
            currentItem.setOtch(middleNameInput.getValue());
            currentItem.setAddress(addressInput.getValue());
            currentItem.setBirthday(datePicker.getValue());
        } else {
            currentItem = new WaiterDto();
            updateCurrentItemValue();
        }
    }

    @Override
    protected boolean checkFields() {
        return super.checkFields();
    }
}