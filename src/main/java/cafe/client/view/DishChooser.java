package cafe.client.view;

import cafe.client.command.LoadCategoriesChooserCommand;
import cafe.client.command.LoadDishesByCategoryAndDateChooserCommand;
import cafe.client.command.LoadDishesByCategoryChooserCommand;
import cafe.client.controller.DishChooserController;
import cafe.client.event.*;
import cafe.client.model.DishChooserModel;
import cafe.client.widgets.CategoryTable;
import cafe.client.widgets.DishTable;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalFooter;

import javax.inject.Inject;
import java.util.Date;

public class DishChooser extends Composite {
    interface DishChooserUiBinder extends UiBinder<HTMLPanel, DishChooser> {
    }

    private final EventBus eventBus;
    private final DishChooserModel dishChooserModel;
    private final DishChooserController dishChooserController;

    private static DishChooserUiBinder ourUiBinder = GWT.create(DishChooserUiBinder.class);

    @UiField
    Modal modalPanel;
    @UiField
    CategoryTable categoryTable;
    @UiField
    DishTable dishTable;
    @UiField
    ModalFooter footerPanel;
    @UiField
    Button submitButton;
    @UiField
    Button cancelButton;

    private Date selectedDate;

    @Inject
    public DishChooser() {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.eventBus = new SimpleEventBus();
        this.dishChooserModel = new DishChooserModel();
        this.dishChooserController = new DishChooserController(eventBus, dishChooserModel);
    }

    public void show() {
        loadCategories();
        selectedDate = null;
        modalPanel.show();
    }

    public void show(Date date) {
        loadCategories();
        selectedDate = date;
        modalPanel.show();
    }


    public void hide() {
        modalPanel.hide();
    }

    public void bind() {
        dishChooserController.bind();

        dishChooserModel.addChangeCategoriesHandler(new ChangeCategoriesEventHandler() {
            @Override
            public void onChangeCategories(ChangeCategoriesEvent event) {
                categoryTable.setRowData(dishChooserModel.getCategories());
            }
        });

        dishChooserModel.addDishesHandler(new ChangeDishesEventHandler() {
            @Override
            public void onChangeDishes(ChangeDishesEvent event) {
                dishTable.setRowData(dishChooserModel.getDishes());
            }
        });

        categoryTable.addChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                dishTable.setVisibleRangeAndClearData(dishTable.getVisibleRange(), true);
                loadDishes(categoryTable.getSelectedCategory());
            }
        });

        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });
    }

    private void loadCategories() {
        eventBus.fireEvent(new LoadCategoriesChooserCommand());
    }

    private void loadDishes(CategoryDto category) {
        if (selectedDate != null) {
            eventBus.fireEvent(new LoadDishesByCategoryAndDateChooserCommand(category, selectedDate.getTime()));
        } else {
            eventBus.fireEvent(new LoadDishesByCategoryChooserCommand(category));
            //eventBus.fireEvent(new LoadDishesByCategoryAndDateChooserCommand(category, new Date().getTime()));
        }
    }

    public void addSubmitHandler(SubmitEventHandler<DishDto> handler) {
        submitButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                DishDto selectedDish = dishTable.getSelectedDish();

                if (selectedDish != null) {
                    handler.onSubmit(selectedDish);
                    hide();
                }
            }
        });
    }
}