package cafe.client.view;

import cafe.client.Console;
import cafe.client.command.LoadWaitersCommand;
import cafe.client.controller.OrderPreviewController;
import cafe.client.model.OrderPreviewModel;
import cafe.client.widgets.IconHtml;
import cafe.client.widgets.IntegerInputBox;
import cafe.client.widgets.OrderDetailTable;
import cafe.shared.dto.OrderDetail;
import cafe.shared.dto.OrderDto;
import cafe.shared.dto.WaiterDto;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.view.client.SingleSelectionModel;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ValueListBox;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.DateTimePicker;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderPreview extends AbstractObjectPreview<OrderDto> {

    interface OrderPreviewUiBinder extends UiBinder<HTMLPanel, OrderPreview> {
    }

    private final EventBus eventBus;
    private final OrderPreviewController orderPreviewController;
    private final OrderPreviewModel orderPreviewModel;

    private static OrderPreviewUiBinder ourUiBinder = GWT.create(OrderPreviewUiBinder.class);

    @UiField
    OrderDetailTable orderDetailTable;
    @UiField
    DateTimePicker dateTimePicker;
    @UiField
    IntegerInputBox numTableInput;
    @UiField(provided = true)
    ValueListBox<WaiterDto> waiterBox;
    @UiField
    Button addDishButton;
    @UiField
    Button deleteDishButton;

    private DishChooser dishChooser;
    private List<OrderDetail> orderDetails;
    private SingleSelectionModel<OrderDetail> singleSelectionModel;

    private OrderDetail currentDetail;

    @Inject
    public OrderPreview() {
        waiterBox = new ValueListBox<>(new AbstractRenderer<WaiterDto>() {
            @Override
            public String render(WaiterDto object) {
                if (object != null) {
                    return object.getFam() + " " + object.getName();
                } else {
                    return "";
                }
            }
        });

        initWidget(ourUiBinder.createAndBindUi(this));

        dishChooser = new DishChooser();
        eventBus = new SimpleEventBus();
        orderPreviewModel = new OrderPreviewModel();
        orderPreviewController = new OrderPreviewController(eventBus, orderPreviewModel);
        singleSelectionModel = new SingleSelectionModel<>();

        ActionCell<OrderDetail> increaseButtonCell = new ActionCell<OrderDetail>(new SafeHtml() {
            @Override
            public String asString() {
                return IconHtml.PLUS;
            }
        }, new ActionCell.Delegate<OrderDetail>() {
            @Override
            public void execute(OrderDetail object) {
                increaseOrderDetailCount(object);
            }
        });

        ActionCell<OrderDetail> decreaseButtonCell = new ActionCell<>(new SafeHtml() {
            @Override
            public String asString() {
                return IconHtml.MINUS;
            }
        }, new ActionCell.Delegate<OrderDetail>() {
            @Override
            public void execute(OrderDetail object) {
                decreaseOrderDetailCount(object);
            }
        });

        Column<OrderDetail, OrderDetail> increaseCol = new Column<OrderDetail, OrderDetail>(increaseButtonCell) {
            @Override
            public OrderDetail getValue(OrderDetail object) {
                return object;
            }
        };

        Column<OrderDetail, OrderDetail> decreaseCol = new Column<OrderDetail, OrderDetail>(decreaseButtonCell) {
            @Override
            public OrderDetail getValue(OrderDetail object) {
                return object;
            }
        };

        decreaseCol.setCellStyleNames("option-button");
        increaseCol.setCellStyleNames("option-button");
        orderDetailTable.addColumn(increaseCol);
        orderDetailTable.addColumn(decreaseCol);
        addDishButton.setIcon(IconType.PLUS);
        deleteDishButton.setIcon(IconType.MINUS);
        orderDetailTable.setSelectionModel(singleSelectionModel);
    }

    @Override
    protected void setAttributes(OrderDto object) {
        if (object != null) {
            setTitle("Заказ номер: " + object.getOrderId());
            dateTimePicker.setValue(object.getDate());
            numTableInput.setValue(object.getNumTable());
            orderDetails.addAll(object.getOrderDetails());
            orderDetailTable.setRowData(orderDetails);
            waiterBox.setValue(object.getWaiter());
        } else {
            setTitle("Новый заказ");
            dateTimePicker.setValue(new Date());
            orderDetailTable.setVisibleRangeAndClearData(orderDetailTable.getVisibleRange(), true);
        }
    }

    @Override
    protected void updateCurrentItemValue() {
        if (currentItem != null) {
            currentItem.setOrderDetails(orderDetails);
            currentItem.setWaiter(waiterBox.getValue());
            currentItem.setNumTable(numTableInput.getValue());
            currentItem.setDate(dateTimePicker.getValue());
            currentItem.setActive(true);
        } else {
            currentItem = new OrderDto();
            updateCurrentItemValue();
        }
    }

    @Override
    public void bind() {
        super.bind();
        dishChooser.bind();
        orderPreviewController.bind();

        addDishButton.addClickHandler(event -> {
            if (currentItem != null && currentItem.getDate() != null) {
                dishChooser.show(currentItem.getDate());
            } else {
                dishChooser.show(new Date());
            }
        });

        dishChooser.addSubmitHandler(object -> {
            dateTimePicker.setEnabled(false);
            int id = currentItem != null ? currentItem.getOrderId() : 0;

            for (OrderDetail od : orderDetails) {
                if (od.getDish().getDishId() == object.getDishId()) {
                    od.setCount(od.getCount() + 1);
                    updateTable();
                    return;
                }
            }
            OrderDetail orderDetail = new OrderDetail(id, object, 1);
            orderDetails.add(orderDetail);
            updateTable();
        });

        orderPreviewModel.addChangeWaitersHandler(event -> {
            if (!orderPreviewModel.getWaiterForChoose().isEmpty()) {
                waiterBox.setValue(orderPreviewModel.getWaiterForChoose().get(0));
                waiterBox.setAcceptableValues(orderPreviewModel.getWaiterForChoose());
            }
        });

        singleSelectionModel.addSelectionChangeHandler(event -> currentDetail = singleSelectionModel.getSelectedObject());

        deleteDishButton.addClickHandler(event -> {
            if (orderDetails.contains(currentDetail)) {
                orderDetails.remove(currentDetail);
                updateTable();
            }
            if (orderDetails.size() == 0) {
                dateTimePicker.setEnabled(true);
            }
        });
    }

    @Override
    public void show() {
        orderDetails = new ArrayList<>();
        fillWaiterBox();
        super.show();
    }

    @Override
    public void show(OrderDto object) {
        orderDetails = new ArrayList<>();
        fillWaiterBox();
        super.show(object);
        updateTable();
    }

    @Override
    protected boolean checkFields() {
        if (orderDetails.size() == 0) {
            Console.write("orderDetails.size()=" + orderDetails.size());
            return false;
        }
        if (waiterBox.getValue() == null) {
            Console.write("waiterBox.getValue()=" + waiterBox.getValue());
            return false;
        }
        if (numTableInput.getValue() == null) {
            Console.write("numTableInput.getValue()=" + numTableInput.getValue());
            return false;
        }
        if (dateTimePicker.getValue() == null) {
            Console.write("dateTimePicker.getValue()=" + dateTimePicker.getValue());
            return false;
        }
        return true;
    }

    private void updateTable() {
        orderDetailTable.setRowData(orderDetails);
    }

    private void fillWaiterBox() {
        eventBus.fireEvent(new LoadWaitersCommand());
    }

    private void increaseOrderDetailCount(OrderDetail currentDetail) {
        currentDetail.setCount(currentDetail.getCount() + 1);
        updateTable();
    }

    private void decreaseOrderDetailCount(OrderDetail currentDetail) {
        if (currentDetail.getCount() > 1) {
            currentDetail.setCount(currentDetail.getCount() - 1);
            updateTable();
        }
    }

}