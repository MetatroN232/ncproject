package cafe.client.view;

import cafe.client.command.*;
import cafe.client.event.*;
import cafe.client.model.MenuListModel;
import cafe.client.widgets.CategoryTable;
import cafe.client.widgets.CustomDialogBox;
import cafe.client.widgets.DishTable;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SelectionChangeEvent;

import javax.inject.Inject;

import static cafe.client.widgets.IconHtml.PLUS;
import static cafe.client.widgets.IconHtml.REFRESH;

public class MenuListView extends Composite {

    interface MenuListViewUiBinder extends UiBinder<HTMLPanel, MenuListView> {
    }

    private static MenuListViewUiBinder ourUiBinder = GWT.create(MenuListViewUiBinder.class);

    @UiField
    HorizontalPanel categoryToolbar;
    @UiField
    HorizontalPanel dishToolbar;
    @UiField
    Grid grid;
    @UiField
    Button refreshButton;
    @UiField
    Button addCategoryButton;
    @UiField
    Button addDishButton;
    @UiField
    TextBox searchBox;
    @UiField
    CategoryTable categoryTable;
    @UiField
    DishTable dishTable;

    final private EventBus eventBus;

    private final MenuListModel menuListModel;

    private final CustomDialogBox addCategoryDialog;
    private final CustomDialogBox addDishDialog;

    private CategoryDto selectedCategory;
    private DishDto selectedDish;

    @Inject
    public MenuListView(final EventBus eventBus, final MenuListModel menuListModel) {
        initWidget(ourUiBinder.createAndBindUi(this));
        this.eventBus = eventBus;
        this.menuListModel = menuListModel;

        addCategoryDialog = new CustomDialogBox();
        addDishDialog = new CustomDialogBox();

        init();
    }

    public void bind() {
        addCategoryDialog.addSubmitHandler(new SubmitEventHandler<String>() {
            @Override
            public void onSubmit(String object) {
                if (!object.equals("")) {
                    CategoryDto categoryDto = new CategoryDto(0, object);
                    createCategory(categoryDto);

                    addCategoryDialog.hide();
                }
            }
        });

        addDishDialog.addSubmitHandler(new SubmitEventHandler<String>() {
            @Override
            public void onSubmit(String name) {
                if (!name.equals("")) {
                    DishDto dish = new DishDto(0, name, selectedCategory.getCategoryId(),0);
                    createDish(dish);
                    addDishDialog.hide();
                }
            }
        });

        menuListModel.addHandler((ChangeCategoriesEventHandler) event -> updateCategoryTable());

        menuListModel.addHandler((ChangeDishesEventHandler) event -> {
            updateDishTable();
        });


        menuListModel.addHandler(new RefreshMenuListEventHandler() {
            @Override
            public void onRefreshMenuList(RefreshMenuListEvent event) {
                refresh();
            }
        });

        categoryTable.addChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                dishTable.setRowCount(0, true);

                selectedCategory = categoryTable.getSelectedCategory();

                eventBus.fireEvent(new LoadDishByCategoryCommand(selectedCategory.getCategoryId()));
            }
        });
        categoryTable.addSubmitUpdateHandler(new SubmitEventHandler<String>() {
            @Override
            public void onSubmit(String object) {
                selectedCategory.setName(object);
                updateCategory(selectedCategory);
            }
        });
        categoryTable.addDeleteHandler(new DeleteCategoryCommandHandler() {
            @Override
            public void onDeleteCategoryCommand(DeleteCategoryCommand event) {
                deleteCategory(event.getCategory());
            }
        });

        dishTable.addChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                selectedDish = dishTable.getSelectedDish();
            }
        });
        dishTable.addSubmitUpdateHandler(new SubmitEventHandler<String>() {
            @Override
            public void onSubmit(String object) {
                selectedDish.setName(object);
                updateDish(selectedDish);
            }
        });
        dishTable.addDeleteHandler(new DeleteDishCommandHandler() {
            @Override
            public void onDeleteDishCommand(DeleteDishCommand event) {
                deleteDish(event.getDish());
            }
        });

        refreshButton.addClickHandler(event -> refresh());

        addDishButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addDishDialog.show();
            }
        });

        addCategoryButton.addClickHandler(event -> {
            addCategoryDialog.show();
        });

        searchBox.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                String name = searchBox.getText();
                eventBus.fireEvent(new FindDishByNameCommand(name));
            }
        });

        refresh();
    }

    private void init() {
        addCategoryDialog.setTitle("Добавление категории");
        addCategoryDialog.setHint("Введите название: ");

        addDishDialog.setTitle("Добавление блюда");
        addDishDialog.setHint("Введите название: ");

        refreshButton.setHTML(REFRESH);
        addCategoryButton.setHTML(PLUS);
        addDishButton.setHTML(PLUS);

        grid.getElement().setId("content");

        searchBox.getElement().setPropertyString("placeholder", "Поиск...");
        searchBox.setTitle("Поиск...");
    }

    private void updateCategoryTable() {
        categoryTable.setRowData(menuListModel.getCategories());
    }

    private void updateDishTable() {
        dishTable.setRowData(menuListModel.getDishes());
    }

    private void refresh() {
        eventBus.fireEvent(new LoadCategoryCommand());
        if (selectedCategory != null) {
            eventBus.fireEvent(new LoadDishByCategoryCommand(selectedCategory.getCategoryId()));
        }
    }

    private void deleteCategory(CategoryDto category) {
        eventBus.fireEvent(new DeleteCategoryCommand(category));
    }

    private void updateCategory(CategoryDto category) {
        eventBus.fireEvent(new UpdateCategoryCommand(category));
    }

    private void createCategory(CategoryDto category) {
        eventBus.fireEvent(new CreateCategoryCommand(category));
    }

    private void createDish(DishDto dish) {
        eventBus.fireEvent(new CreateDishCommand(dish));
    }

    private void updateDish(DishDto dish) {
        eventBus.fireEvent(new UpdateDishCommand(dish));
    }

    private void deleteDish(DishDto dish) {
        eventBus.fireEvent(new DeleteDishCommand(dish));
    }
}