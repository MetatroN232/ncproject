package cafe.client.view;

import cafe.shared.dto.DishDto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import org.gwtbootstrap3.client.ui.Modal;

import java.util.ArrayList;
import java.util.List;

public class TestView extends Composite {
    interface TestViewUiBinder extends UiBinder<HTMLPanel, TestView> {
    }

    private static TestViewUiBinder ourUiBinder = GWT.create(TestViewUiBinder.class);
    @UiField
    Modal modalPanel;

    public TestView() {
        initWidget(ourUiBinder.createAndBindUi(this));
        List<DishDto> dishDtos = new ArrayList<>();
    }

    public void show() {
        modalPanel.show();
    }
}