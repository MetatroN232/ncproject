package cafe.client;

import cafe.client.config.Injector;
import cafe.client.controller.LifeCycle;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import org.fusesource.restygwt.client.Defaults;

public class Cafe implements EntryPoint {
    static {
        Defaults.setServiceRoot(GWT.getHostPageBaseURL());
        Defaults.setDateFormat(null);
    }
    public void onModuleLoad() {
        Injector injector = GWT.create(Injector.class);

        LifeCycle lifeCycle = injector.getLifeCycle();
        lifeCycle.start();
    }
}