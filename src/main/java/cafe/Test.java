package cafe;

import cafe.server.dao.impl.OrderDaoImpl;
import cafe.server.entity.Order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("active", true);
        map.put("numTable", 5);

        OrderDaoImpl dao = new OrderDaoImpl();

        List<Order> filter = dao.filter(map);

        for (Order order : filter) {
            System.out.println(order);
        }
    }

}
