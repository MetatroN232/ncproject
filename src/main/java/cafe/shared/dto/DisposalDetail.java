package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class DisposalDetail implements Serializable {
    private int disposalId;
    private DishDto dish;

    public DisposalDetail() {
    }

    @JsonCreator
    public DisposalDetail(@JsonProperty("disposalId") int disposalId,
                          @JsonProperty("dish") DishDto dish) {
        this.disposalId = disposalId;
        this.dish = dish;
    }

    public int getDisposalId() {
        return disposalId;
    }

    public void setDisposalId(int disposalId) {
        this.disposalId = disposalId;
    }

    public DishDto getDish() {
        return dish;
    }

    public void setDish(DishDto dish) {
        this.dish = dish;
    }
}
