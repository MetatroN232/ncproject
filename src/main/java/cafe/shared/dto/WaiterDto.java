package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;


public class WaiterDto implements Serializable{

    private int waiterId;
    private String name;
    private String fam;
    private String otch;
    private Date birthday;
    private String address;

    public WaiterDto() {
    }

    @JsonCreator
    public WaiterDto(@JsonProperty("waiterId") int waiterId,
                     @JsonProperty("name") String name,
                     @JsonProperty("fam") String fam,
                     @JsonProperty("otch") String otch,
                     @JsonProperty("birthday") Date birthday,
                     @JsonProperty("address") String address) {
        this.waiterId = waiterId;
        this.name = name;
        this.fam = fam;
        this.otch = otch;
        this.birthday = birthday;
        this.address = address;
    }

    public int getWaiterId() {
        return waiterId;
    }

    public void setWaiterId(int waiterId) {
        this.waiterId = waiterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFam() {
        return fam;
    }

    public void setFam(String fam) {
        this.fam = fam;
    }

    public String getOtch() {
        return otch;
    }

    public void setOtch(String otch) {
        this.otch = otch;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
