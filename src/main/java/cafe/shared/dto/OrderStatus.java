package cafe.shared.dto;

public enum OrderStatus {
    ALL("Все", null),

    OPEN("Активные", true),

    CLOSE("Закрытые", false);

    private String text;
    private Boolean status;

    OrderStatus(String text, Boolean status) {
        this.text = text;
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public Boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "OrderStatus{" +
                "text='" + text + '\'' +
                ", status=" + status +
                '}';
    }
}
