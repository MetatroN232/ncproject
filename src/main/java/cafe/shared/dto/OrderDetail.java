package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class OrderDetail implements Serializable {

    private int orderId;
    private DishDto dish;
    private int count;

    public OrderDetail() {
    }

    @JsonCreator
    public OrderDetail(
            @JsonProperty("orderId") int orderId,
            @JsonProperty("dish") DishDto dish,
            @JsonProperty("count") int count) {
        this.orderId = orderId;
        this.dish = dish;
        this.count = count;
    }

    public DishDto getDish() {
        return dish;
    }

    public void setDish(DishDto dish) {
        this.dish = dish;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
