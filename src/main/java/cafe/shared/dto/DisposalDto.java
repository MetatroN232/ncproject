package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DisposalDto implements Serializable{

    private int disposalId;
    private int numDisposal;
    private Date date;
    private List<DisposalDetail> disposalDetails;

    public DisposalDto() {
    }

    @JsonCreator
    public DisposalDto(
            @JsonProperty("disposalId") int disposalId,
            @JsonProperty("numDisposal") int numDisposal,
            @JsonProperty("date") Date date,
            @JsonProperty("disposalDetails") List<DisposalDetail> disposalDetails) {
        this.disposalId = disposalId;
        this.numDisposal = numDisposal;
        this.date = date;
        this.disposalDetails = disposalDetails;
    }

    public int getDisposalId() {
        return disposalId;
    }

    public void setDisposalId(int disposalId) {
        this.disposalId = disposalId;
    }

    public int getNumDisposal() {
        return numDisposal;
    }

    public void setNumDisposal(int numDisposal) {
        this.numDisposal = numDisposal;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<DisposalDetail> getDisposalDetails() {
        return disposalDetails;
    }

    public void setDisposalDetails(List<DisposalDetail> disposalDetails) {
        this.disposalDetails = disposalDetails;
    }
}
