package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class DishDto implements Serializable {

    private int dishId;
    private String name;
    private int categoryId;
    private int price;

    public DishDto() {
    }

    public DishDto(int dishId) {
        this.dishId = dishId;
    }

    public DishDto(int i, int j) {}

    @JsonCreator
    public DishDto(
            @JsonProperty("dishId") Integer dishId,
            @JsonProperty("name") String name,
            @JsonProperty("categoryId") Integer categoryId,
            @JsonProperty("price") Integer price) {
        this.dishId = dishId;
        this.name = name;
        this.categoryId = categoryId;
        this.price = price;
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
