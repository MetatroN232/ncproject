package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CategoryDto implements Serializable {
    private int categoryId;
    private String nameCategory;

    public CategoryDto() {
    }

    public CategoryDto(int categoryId) {
        this.categoryId = categoryId;
    }

    @JsonCreator
    public CategoryDto(
            @JsonProperty("categoryId") int categoryId,
            @JsonProperty("nameCategory") String nameCategory) {
        this.categoryId = categoryId;
        this.nameCategory = nameCategory;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return nameCategory;
    }

    public void setName(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDto that = (CategoryDto) o;

        if (categoryId != that.categoryId) return false;
        return nameCategory != null ? nameCategory.equals(that.nameCategory) : that.nameCategory == null;
    }

    @Override
    public int hashCode() {
        int result = categoryId;
        result = 31 * result + (nameCategory != null ? nameCategory.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "categoryId=" + categoryId +
                ", nameCategory='" + nameCategory + '\'' +
                '}';
    }
}
