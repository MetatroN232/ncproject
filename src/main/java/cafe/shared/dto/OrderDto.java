package cafe.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrderDto implements Serializable {

    private int orderId;
    private Date date;
    private int numTable;
    private WaiterDto waiter;
    private List<OrderDetail> orderDetails;
    private boolean active;

    public OrderDto() {
    }

    public OrderDto(@JsonProperty("orderId") int orderId,
                    @JsonProperty("date") Date date,
                    @JsonProperty("numTable") int numTable,
                    @JsonProperty("waiter") WaiterDto waiter,
                    @JsonProperty("orderDetails") List<OrderDetail> orderDetails,
                    @JsonProperty("active") boolean active) {
        this.orderId = orderId;
        this.date = date;
        this.numTable = numTable;
        this.waiter = waiter;
        this.orderDetails = orderDetails;
        this.active = active;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNumTable() {
        return numTable;
    }

    public void setNumTable(int numTable) {
        this.numTable = numTable;
    }

    public WaiterDto getWaiter() {
        return waiter;
    }

    public void setWaiter(WaiterDto waiter) {
        this.waiter = waiter;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
