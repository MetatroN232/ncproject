package cafe.server.service;

import cafe.server.dao.DishDao;
import cafe.server.dao.DisposalDao;
import cafe.server.dao.OrderDao;
import cafe.server.dao.impl.DishDaoImpl;
import cafe.server.dao.impl.DisposalDaoImpl;
import cafe.server.dao.impl.OrderDaoImpl;
import cafe.server.entity.DishHasDisposal;
import cafe.server.entity.Order;
import cafe.server.entity.OrderHasDish;
import cafe.server.entity.Waiter;
import cafe.server.entity.wrapper.DishWrapper;
import cafe.server.util.ReportBuilder;
import cafe.server.util.mapper.DishPropertyMap;
import cafe.server.util.mapper.Mapper;
import cafe.shared.dto.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Path("/orders")
public class OrderService {

    private OrderDao orderDao = new OrderDaoImpl();
    private DishDao dishDao = new DishDaoImpl();
    private DisposalDao disposalDao = new DisposalDaoImpl();

    private final Mapper<Order, OrderDto> orderMapper;
    private final Mapper<DishWrapper, DishDto> dishMapper;
    private final Mapper<Waiter, WaiterDto> waiterMapper;

    public OrderService() {
        orderMapper = new Mapper<>(Order.class, OrderDto.class);
        dishMapper = new Mapper<>(DishWrapper.class, DishDto.class);
        waiterMapper = new Mapper<>(Waiter.class, WaiterDto.class);

        dishMapper.addProperty(new DishPropertyMap());
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<OrderDto> getOrders() {
        List<OrderDto> result = new ArrayList<>();
        //List<Order> orders = orderDao.filterAndSort(map);
        List<Order> orders = orderDao.getAll();
        for (Order order : orders) {
            result.add(getOrderDtoByOrder(order));
        }
        return result;
    }

    @GET
    @Path("/reports/{month}/{year}")
    @Produces("application/pdf")
    public Response getOrdersByMonth(@PathParam("month") int month,
                                    @PathParam("year") int year) {
        List<OrderDto> result = new ArrayList<>();
        List<Order> orders = orderDao.ordersByMonth(month, year);
        for (Order order : orders) {
            result.add(getOrderDtoByOrder(order));
        }

        Month monthEnum = null;

        for (Month m : Month.values()) {
            if (m.getNumber() == month)
                monthEnum = m;
        }
        File file = ReportBuilder.buildMonthReport(result, monthEnum, year);

        return Response.ok(file)/*.header("Content-Disposition",
                "attachment; filename=order.pdf")*/.build();
    }

    @GET
    @Path("/reports/check{orderId}")
    @Produces("application/pdf")
    public Response getOrderCheck(@PathParam("orderId") int orderId) {
        Order order = orderDao.find(orderId);
        OrderDto dto = getOrderDtoByOrder(order);

        File file = ReportBuilder.buildCheck(dto);

        return Response.ok(file)/*.header("Content-Disposition",
                "attachment; filename=order.pdf")*/.build();
    }

    @GET
    @Path("/reports/{dateLong}")
    @Produces("application/pdf")
    public Response getOrdersByDay(@PathParam("dateLong") long dateLong) {

        List<OrderDto> result = new ArrayList<>();
        Date date = new Date(dateLong);
        List<Order> orders = orderDao.ordersByDay(date);
        for (Order order : orders) {
            result.add(getOrderDtoByOrder(order));
        }

        File file = ReportBuilder.buildDayReport(result, date);

        return Response.ok(file)/*.header("Content-Disposition",
                "attachment; filename=order.pdf")*/.build();
    }

    @POST
    @Path("/filterAndSort/{sort}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getOrdersByFilter(Map<String, Object> filterMap, @PathParam("sort") String sort) {
        List<OrderDto> result = new ArrayList<>();
        //List<Order> orders = orderDao.filterAndSort(map);
        List<Order> orders = filterMap == null ? orderDao.getAll() : orderDao.filterAndSort(filterMap, sort);

        for (Order order : orders) {
            result.add(getOrderDtoByOrder(order));
        }

        return Response.ok().entity(result).build();
    }

    @GET
    @Path("/{orderId}")
    @Produces(MediaType.APPLICATION_JSON)
    public OrderDto getOrderById(@PathParam("orderId") int orderId) {
        Order order = orderDao.find(orderId);
        return getOrderDtoByOrder(order);
    }



    @GET
    @Path("/{orderId}/waiter")
    @Produces(MediaType.APPLICATION_JSON)
    public WaiterDto getWaiterByOrder(@PathParam("orderId") int orderId) {
        Waiter waiter = orderDao.getWaiterByOrderId(orderId);
        return waiterMapper.createDto(waiter);
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createOrder(OrderDto orderDto) {
        //TODO create order
        Order order = orderMapper.createEntity(orderDto);
        orderDao.save(order);

        for (OrderDetail od : orderDto.getOrderDetails()) {
            OrderHasDish orderHasDish = new OrderHasDish(
                    order.getOrderId(), od.getDish().getDishId(), od.getCount());

            orderDao.saveOrderDetail(orderHasDish);
        }

        return Response.ok().entity("ok").build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOrder(OrderDto orderDto) {
        //TODO update order
        Order order = orderMapper.createEntity(orderDto);
        orderDao.update(order);
        orderDao.clearOrder(order);

        for (OrderDetail od : orderDto.getOrderDetails()) {
            OrderHasDish orderHasDish = new OrderHasDish(
                    order.getOrderId(), od.getDish().getDishId(), od.getCount());
            orderDao.saveOrderDetail(orderHasDish);
        }
        return Response.ok().build();
    }

    @PUT
    @Path("/{orderId}")
    public Response setStatus(@PathParam("orderId") int orderId,
                              @QueryParam("status") boolean status) {
        orderDao.setStatus(orderId, status);
        return Response.ok().build();
    }

    @DELETE
    @Consumes
    public Response deleteOrder(OrderDto orderDto) {
        //TODO delete order
        Order order = orderMapper.createEntity(orderDto);
        orderDao.delete(order);
        return Response.ok().build();
    }

    private OrderDto getOrderDtoByOrder(Order order) {
        List<OrderHasDish> orderHasDishes = orderDao.getDishes(order.getOrderId());
        List<OrderDetail> orderDetails = new ArrayList<>();
        for (OrderHasDish od : orderHasDishes) {
            DishHasDisposal dishHasDisposal = disposalDao.getPriceByOrder(od.getDishDishId(), order.getOrderId());
            DishWrapper wrapper = new DishWrapper(dishDao.find(dishHasDisposal.getDishDishId()), dishHasDisposal);
            OrderDetail detail = new OrderDetail(order.getOrderId(), dishMapper.createDto(wrapper), od.getCount());
            orderDetails.add(detail);
        }

        OrderDto result = new OrderDto(order.getOrderId(),
                order.getDate(),
                order.getNumTable(),
                waiterMapper.createDto(orderDao.getWaiterByOrderId(order.getOrderId())),
                orderDetails, order.isActive());
        return result;
    }

}
