package cafe.server.service;

import cafe.server.dao.DisposalDao;
import cafe.server.dao.impl.DisposalDaoImpl;
import cafe.server.entity.Dish;
import cafe.server.entity.DishHasDisposal;
import cafe.server.entity.Disposal;
import cafe.server.entity.wrapper.DishWrapper;
import cafe.server.util.mapper.DishPropertyMap;
import cafe.server.util.mapper.Mapper;
import cafe.shared.dto.DishDto;
import cafe.shared.dto.DisposalDetail;
import cafe.shared.dto.DisposalDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/disposals")
public class DisposalService {

    private final DisposalDao disposalDao;

    private final Mapper<DishWrapper, DishDto> dishMapper;
    Mapper<Disposal, DisposalDto> disposalMapper;


    public DisposalService() {
        disposalDao = new DisposalDaoImpl();

        dishMapper = new Mapper<>(DishWrapper.class, DishDto.class);
        disposalMapper = new Mapper<>(Disposal.class, DisposalDto.class);

        dishMapper.addProperty(new DishPropertyMap());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<DisposalDto> getDisposals() {
        List<Disposal> disposals = disposalDao.getAll();
        List<DisposalDto> result = new ArrayList<>();
        for (Disposal d : disposals) {
            result.add(getDisposalDto(d));
        }
        return result;
    }

    @GET
    @Path("/{disposalId}")
    @Produces(MediaType.APPLICATION_JSON)
    public DisposalDto getDisposal(@PathParam("disposalId") int disposalId) {
        Disposal disposal = disposalDao.find(disposalId);
        return getDisposalDto(disposal);
    }

    @GET
    @Path("/{disposalId}/dishes")
    @Produces
    public List<DishDto> getDishesByDisposal(@PathParam("disposalId") int disposalId) {
        List<Dish> dishes = disposalDao.getDishes(disposalId);
        List<DishDto> result = new ArrayList<>();

        for (Dish d : dishes) {
            result.add(new DishDto(d.getDishId(),
                    d.getName(),
                    d.getCategoryId(),
                    disposalDao.getPriceByDisposal(disposalId, d.getDishId()).getPrice()));
        }
        return result;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDisposal(DisposalDto disposalDto) {
        //TODO create disposal
        Disposal disposal = disposalMapper.createEntity(disposalDto);
        disposalDao.save(disposal);

        for (DisposalDetail disposalDetail : disposalDto.getDisposalDetails()) {
            DishHasDisposal dishHasDisposal = new DishHasDisposal();
            dishHasDisposal.setPrice(disposalDetail.getDish().getPrice());
            dishHasDisposal.setDishDishId(disposalDetail.getDish().getDishId());
            dishHasDisposal.setDisposalDisposalId(disposal.getDisposalId());

            disposalDao.savePrice(dishHasDisposal);
        }

        return Response.ok().build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDisposal(DisposalDto disposalDto) {
        //TODO update disposal

        Disposal disposal = disposalMapper.createEntity(disposalDto);
        disposalDao.update(disposal);
        disposalDao.clearDisposal(disposal);

        for (DisposalDetail dd : disposalDto.getDisposalDetails()) {
            DishHasDisposal dishHasDisposal = new DishHasDisposal(
                    dd.getDish().getDishId(), disposal.getDisposalId(), dd.getDish().getPrice());
            disposalDao.savePrice(dishHasDisposal);
        }

        return Response.ok().build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteDisposal(DisposalDto disposalDto) {
        //TODO delete disposal
        Disposal disposal = disposalMapper.createEntity(disposalDto);
        disposalDao.delete(disposal);
        return Response.ok().build();
    }

    private DisposalDto getDisposalDto(Disposal disposal) {
        List<Dish> dishes = disposalDao.getDishes(disposal.getDisposalId());
        List<DisposalDetail> disposalDetails = new ArrayList<>();
        for (Dish d : dishes) {
            DishHasDisposal disposalPrice = disposalDao.getPriceByDisposal(disposal.getDisposalId(), d.getDishId());
            DishWrapper wrapper = new DishWrapper(d, disposalPrice);
            disposalDetails.add(new DisposalDetail(disposal.getDisposalId(), dishMapper.createDto(wrapper)));
        }

        return new DisposalDto(disposal.getDisposalId(),
                disposal.getNumDisposal(),
                disposal.getDate(),
                disposalDetails);
    }

}
