package cafe.server.service;

import cafe.server.dao.WaiterDao;
import cafe.server.dao.impl.WaiterDaoImpl;
import cafe.server.entity.Waiter;
import cafe.server.util.mapper.Mapper;
import cafe.shared.dto.WaiterDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/waiters")
public class WaiterService {

    private final Mapper<Waiter, WaiterDto> waiterMapper;

    private final WaiterDao waiterDao;

    public WaiterService() {
        waiterDao = new WaiterDaoImpl();
        waiterMapper = new Mapper<>(Waiter.class, WaiterDto.class);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<WaiterDto> getWaiters() {
        List<Waiter> list = waiterDao.getAll();
        return waiterMapper.createDtoList(list);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createWaiter(WaiterDto waiterDto) {
        Waiter waiter = waiterMapper.createEntity(waiterDto);
        waiterDao.save(waiter);
        return Response.ok().build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateWaiter(WaiterDto waiterDto) {
        Waiter waiter = waiterMapper.createEntity(waiterDto);
        waiterDao.update(waiter);
        return Response.ok().build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteWaiter(WaiterDto waiterDto) {
        Waiter waiter = waiterMapper.createEntity(waiterDto);
        waiterDao.delete(waiter);
        return Response.ok().build();
    }

    @GET
    @Path("/{waiterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public WaiterDto getWaiter(@PathParam("waiterId") int waiterId) {
        Waiter waiter = waiterDao.find(waiterId);
        return waiterMapper.createDto(waiter);
    }
}
