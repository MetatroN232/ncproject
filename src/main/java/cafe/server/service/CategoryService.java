package cafe.server.service;

import cafe.server.dao.CategoryDao;
import cafe.server.dao.DisposalDao;
import cafe.server.dao.impl.CategoryDaoImpl;
import cafe.server.dao.impl.DisposalDaoImpl;
import cafe.server.entity.Category;
import cafe.server.entity.Dish;
import cafe.server.entity.DishHasDisposal;
import cafe.server.entity.wrapper.DishWrapper;
import cafe.server.util.mapper.DishPropertyMap;
import cafe.server.util.mapper.Mapper;
import cafe.shared.dto.CategoryDto;
import cafe.shared.dto.DishDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/categories")
public class CategoryService {

    private final CategoryDao categoryDao;
    private final DisposalDao disposalDao;

    private final Mapper<Category, CategoryDto> categoryMapper;
    private final Mapper<DishWrapper, DishDto> dishMapper;

    public CategoryService() {
        categoryDao = new CategoryDaoImpl();
        disposalDao = new DisposalDaoImpl();

        categoryMapper = new Mapper<>(Category.class, CategoryDto.class);
        dishMapper = new Mapper<>(DishWrapper.class, DishDto.class);
        dishMapper.addProperty(new DishPropertyMap());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CategoryDto> getCategories() {
        return categoryMapper.createDtoList(categoryDao.getAll());
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public CategoryDto getCategoryById(@PathParam("id") int categoryId) {
        return categoryMapper.createDto(categoryDao.find(categoryId));
    }

    @GET
    @Path("/{categoryId}/dishes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DishDto> getDishesByCategory(@PathParam("categoryId") int categoryId,
                                             @QueryParam("date") Long date) {
        List<DishDto> result = new ArrayList<>();
        List<Dish> dishesByCategory = categoryDao.getDishesByCategory(categoryId);

        if (date != null) {
            for (Dish d : dishesByCategory) {
                Date date1 = new Date(date);
                DishHasDisposal priceByDate = disposalDao.getPriceByDate(d.getDishId(), date1);

                if (priceByDate != null) {
                    DishWrapper wrapper = new DishWrapper(d, priceByDate);
                    result.add(dishMapper.createDto(wrapper));
                }
            }
        } else {
            for (Dish d : dishesByCategory) {
                DishWrapper wrapper = new DishWrapper(d, disposalDao.getActualPrice(d.getDishId()));
                result.add(dishMapper.createDto(wrapper));
            }
        }

        return result;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addCategory(CategoryDto categoryDto) {
        Category category = categoryMapper.createEntity(categoryDto);
        categoryDao.save(category);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateCategory(CategoryDto categoryDto) {
        Category category = categoryMapper.createEntity(categoryDto);
        categoryDao.update(category);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteCategory(CategoryDto categoryDto) {
        Category category = categoryMapper.createEntity(categoryDto);
        categoryDao.delete(category);
    }
}
