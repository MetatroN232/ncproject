package cafe.server.service;

import cafe.server.dao.DishDao;
import cafe.server.dao.DisposalDao;
import cafe.server.dao.impl.DishDaoImpl;
import cafe.server.dao.impl.DisposalDaoImpl;
import cafe.server.entity.Dish;
import cafe.server.entity.DishHasDisposal;
import cafe.server.entity.wrapper.DishWrapper;
import cafe.server.util.mapper.DishPropertyMap;
import cafe.server.util.mapper.Mapper;
import cafe.shared.dto.DishDto;
import org.hibernate.HibernateException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/dishes")
public class DishService {

    private final DishDao dishDao;
    private final DisposalDao disposalDao;

    private final Mapper<DishWrapper, DishDto> dishMapper;
    //private final Mapper<DishHasDisposal, DishHasDisposalDto> dishHasDisposalMapper = new Mapper<>(DishHasDisposal.class, DishHasDisposalDto.class);

    /*@GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<DishDto> getDishes() {
        List<Dish> dishes = dishDao.getAll();
        return dishMapper.createDtoList(dishes);
    }*/

    public DishService() {
        dishDao = new DishDaoImpl();
        disposalDao = new DisposalDaoImpl();

        dishMapper = new Mapper<>(DishWrapper.class, DishDto.class);
        dishMapper.addProperty(new DishPropertyMap());
    }

    @GET
    @Path("/{dishId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDish(@PathParam("dishId") int dishId) {
        Dish dish = dishDao.find(dishId);
        DishHasDisposal dishHasDisposal = disposalDao.getActualPrice(dish.getDishId());
        DishWrapper wrapper = new DishWrapper(dish, dishHasDisposal);
        DishDto result = dishMapper.createDto(wrapper);
        return Response.ok().entity(result).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findDishByName(@QueryParam("name") String name) {
        List<DishDto> result = new ArrayList<>();
        List<Dish> dishes;

        if (name == null || name.equals("")) {
            dishes = dishDao.getAll();
        } else {
            dishes = dishDao.findByName(name);
        }

        for (Dish d : dishes) {
            DishWrapper wrapper = new DishWrapper(d, disposalDao.getActualPrice(d.getDishId()));
            result.add(dishMapper.createDto(wrapper));
        }

        return Response.ok().entity(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDish(DishDto dishDto) {
        DishWrapper entity = dishMapper.createEntity(dishDto);
        try {
            dishDao.save(entity.getDish());
            return Response.ok().build();
        } catch (HibernateException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDish(DishDto dishDto) {
        DishWrapper entity = dishMapper.createEntity(dishDto);

        try {
            dishDao.update(entity.getDish());
            return Response.ok().build();
        } catch (HibernateException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteDish(DishDto dishDto) {
        DishWrapper entity = dishMapper.createEntity(dishDto);

        try {
            dishDao.delete(entity.getDish());
            return Response.ok().build();
        } catch (HibernateException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    /*@GET
    @Path("/{dishId}/getActualPrice")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getActualPrice(@PathParam("dishId") int dishId) {
        DishHasDisposal dishHasDisposal = dishDao.getActualPrice(dishId);

        if (dishHasDisposal == null) {
            return Response.status(Status.NOT_FOUND).entity("No disposal for this dish").buildCheck();
        } else {
            DishHasDisposalDto dto = dishHasDisposalMapper.createDto(dishHasDisposal);
            Response.ResponseBuilder response = Response.ok().entity(dto);
            return response.buildCheck();
        }
    }

    @GET
    @Path("/{dishId}/getPrice")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPriceByOrder(@PathParam("dishId") int dishId, @QueryParam("orderId") int orderId) {
        DishHasDisposal price = dishDao.getPriceByOrder(dishId, orderId);
        DishHasDisposalDto dto = dishHasDisposalMapper.createDto(price);
        Response.ResponseBuilder response = Response.ok().entity(dto);
        return response.buildCheck();
    }*/
}
