package cafe.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "\"Order\"")
public class Order {
    private int orderId;
    private Date date;
    private int numTable;
    private int waiterId;
    private boolean active;
    @JsonIgnore
    private Waiter waiterByWaiterId;
    @JsonIgnore
    private Collection<OrderHasDish> orderHasDishesByOrderId;

    public Order() {
    }

    public Order(int orderId, Date date, int numTable, int waiterId) {
        this.orderId = orderId;
        this.date = date;
        this.numTable = numTable;
        this.waiterId = waiterId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id", nullable = false)
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "num_table", nullable = false)
    public int getNumTable() {
        return numTable;
    }

    public void setNumTable(int numTable) {
        this.numTable = numTable;
    }

    @Basic
    @Column(name = "waiter_id", nullable = false)
    public int getWaiterId() {
        return waiterId;
    }

    public void setWaiterId(int waiterId) {
        this.waiterId = waiterId;
    }

    @Basic
    @Column(name = "status")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (orderId != order.orderId) return false;
        if (numTable != order.numTable) return false;
        if (waiterId != order.waiterId) return false;
        if (date != null ? !date.equals(order.date) : order.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + numTable;
        result = 31 * result + waiterId;
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", date=" + date +
                ", numTable=" + numTable +
                ", waiterId=" + waiterId +
                ", active=" + active +
                '}';
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "waiter_id", referencedColumnName = "waiter_id", nullable = false, insertable = false, updatable = false)
    public Waiter getWaiterByWaiterId() {
        return waiterByWaiterId;
    }

    public void setWaiterByWaiterId(Waiter waiterByWaiterId) {
        this.waiterByWaiterId = waiterByWaiterId;
    }

    @OneToMany(mappedBy = "orderByOrderOrderId", fetch = FetchType.LAZY)
    public Collection<OrderHasDish> getOrderHasDishesByOrderId() {
        return orderHasDishesByOrderId;
    }

    public void setOrderHasDishesByOrderId(Collection<OrderHasDish> orderHasDishesByOrderId) {
        this.orderHasDishesByOrderId = orderHasDishesByOrderId;
    }
}
