package cafe.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Waiter {
    private int waiterId;
    private String name;
    private String fam;
    private String otch;
    private Date birthday;
    private String address;
    @JsonIgnore
    private Collection<Order> ordersByWaiterId;

    @Id
    @Column(name = "waiter_id", nullable = false)
    public int getWaiterId() {
        return waiterId;
    }

    public void setWaiterId(int waiterId) {
        this.waiterId = waiterId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "fam", nullable = false, length = 45)
    public String getFam() {
        return fam;
    }

    public void setFam(String fam) {
        this.fam = fam;
    }

    @Basic
    @Column(name = "otch", length = 45)
    public String getOtch() {
        return otch;
    }

    public void setOtch(String otch) {
        this.otch = otch;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "address", length = 45)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Waiter waiter = (Waiter) o;

        if (waiterId != waiter.waiterId) return false;
        if (name != null ? !name.equals(waiter.name) : waiter.name != null) return false;
        if (fam != null ? !fam.equals(waiter.fam) : waiter.fam != null) return false;
        if (otch != null ? !otch.equals(waiter.otch) : waiter.otch != null) return false;
        if (birthday != null ? !birthday.equals(waiter.birthday) : waiter.birthday != null) return false;
        if (address != null ? !address.equals(waiter.address) : waiter.address != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = waiterId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (fam != null ? fam.hashCode() : 0);
        result = 31 * result + (otch != null ? otch.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "waiterByWaiterId", fetch = FetchType.LAZY)
    public Collection<Order> getOrdersByWaiterId() {
        return ordersByWaiterId;
    }

    public void setOrdersByWaiterId(Collection<Order> ordersByWaiterId) {
        this.ordersByWaiterId = ordersByWaiterId;
    }
}
