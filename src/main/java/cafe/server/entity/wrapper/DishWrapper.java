package cafe.server.entity.wrapper;

import cafe.server.entity.Dish;
import cafe.server.entity.DishHasDisposal;

public class DishWrapper {
    private Dish dish;
    private DishHasDisposal dishHasDisposal;

    public DishWrapper() {
    }

    public DishWrapper(Dish dish, DishHasDisposal dishHasDisposal) {
        this.dish = dish;
        this.dishHasDisposal = dishHasDisposal;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public DishHasDisposal getDishHasDisposal() {
        return dishHasDisposal;
    }

    public void setDishHasDisposal(DishHasDisposal dishHasDisposal) {
        this.dishHasDisposal = dishHasDisposal;
    }
}
