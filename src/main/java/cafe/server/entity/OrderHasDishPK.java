package cafe.server.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class OrderHasDishPK implements Serializable {
    private int orderOrderId;
    private int dishDishId;

    @Column(name = "order_order_id", nullable = false)
    @Id
    public int getOrderOrderId() {
        return orderOrderId;
    }

    public void setOrderOrderId(int orderOrderId) {
        this.orderOrderId = orderOrderId;
    }

    @Column(name = "dish_dish_id", nullable = false)
    @Id
    public int getDishDishId() {
        return dishDishId;
    }

    public void setDishDishId(int dishDishId) {
        this.dishDishId = dishDishId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderHasDishPK that = (OrderHasDishPK) o;

        if (orderOrderId != that.orderOrderId) return false;
        if (dishDishId != that.dishDishId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderOrderId;
        result = 31 * result + dishDishId;
        return result;
    }
}
