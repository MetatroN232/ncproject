package cafe.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "dish_has_disposal")
@IdClass(DishHasDisposalPK.class)
public class DishHasDisposal {
    private int dishDishId;
    private int disposalDisposalId;
    private int price;
    @JsonIgnore
    private Dish dishByDishDishId;
    @JsonIgnore
    private Disposal disposalByDisposalDisposalId;

    public DishHasDisposal() {
    }

    public DishHasDisposal(int dishDishId, int disposalDisposalId, int price) {
        this.dishDishId = dishDishId;
        this.disposalDisposalId = disposalDisposalId;
        this.price = price;
    }

    @Id
    @Column(name = "dish_dish_id", nullable = false)
    public int getDishDishId() {
        return dishDishId;
    }

    public void setDishDishId(int dishDishId) {
        this.dishDishId = dishDishId;
    }

    @Id
    @Column(name = "disposal_disposal_id", nullable = false)
    public int getDisposalDisposalId() {
        return disposalDisposalId;
    }

    public void setDisposalDisposalId(int disposalDisposalId) {
        this.disposalDisposalId = disposalDisposalId;
    }

    @Basic
    @Column(name = "price", nullable = false)
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DishHasDisposal that = (DishHasDisposal) o;

        if (dishDishId != that.dishDishId) return false;
        if (disposalDisposalId != that.disposalDisposalId) return false;
        if (price != that.price) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dishDishId;
        result = 31 * result + disposalDisposalId;
        result = 31 * result + price;
        return result;
    }

    @Override
    public String toString() {
        return "DishHasDisposal{" +
                "dishDishId=" + dishDishId +
                ", disposalDisposalId=" + disposalDisposalId +
                ", price=" + price +
                '}';
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dish_dish_id", referencedColumnName = "dish_id", nullable = false, insertable = false, updatable = false)
    public Dish getDishByDishDishId() {
        return dishByDishDishId;
    }

    public void setDishByDishDishId(Dish dishByDishDishId) {
        this.dishByDishDishId = dishByDishDishId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "disposal_disposal_id", referencedColumnName = "disposal_id", nullable = false, insertable = false, updatable = false)
    public Disposal getDisposalByDisposalDisposalId() {
        return disposalByDisposalDisposalId;
    }

    public void setDisposalByDisposalDisposalId(Disposal disposalByDisposalDisposalId) {
        this.disposalByDisposalDisposalId = disposalByDisposalDisposalId;
    }
}
