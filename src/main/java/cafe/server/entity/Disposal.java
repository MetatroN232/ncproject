package cafe.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Disposal {
    private int disposalId;
    private int numDisposal;
    private Date date;
    @JsonIgnore
    private Collection<DishHasDisposal> dishHasDisposalsByDisposalId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "disposal_id", nullable = false)
    public int getDisposalId() {
        return disposalId;
    }

    public void setDisposalId(int disposalId) {
        this.disposalId = disposalId;
    }

    @Basic
    @Column(name = "num_disposal", nullable = false)
    public int getNumDisposal() {
        return numDisposal;
    }

    public void setNumDisposal(int numDisposal) {
        this.numDisposal = numDisposal;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Disposal disposal = (Disposal) o;

        if (disposalId != disposal.disposalId) return false;
        if (numDisposal != disposal.numDisposal) return false;
        if (date != null ? !date.equals(disposal.date) : disposal.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = disposalId;
        result = 31 * result + numDisposal;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "disposalByDisposalDisposalId", fetch = FetchType.LAZY)
    public Collection<DishHasDisposal> getDishHasDisposalsByDisposalId() {
        return dishHasDisposalsByDisposalId;
    }

    public void setDishHasDisposalsByDisposalId(Collection<DishHasDisposal> dishHasDisposalsByDisposalId) {
        this.dishHasDisposalsByDisposalId = dishHasDisposalsByDisposalId;
    }
}
