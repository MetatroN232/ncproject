package cafe.server.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DishHasDisposalPK implements Serializable {
    private int dishDishId;
    private int disposalDisposalId;

    public DishHasDisposalPK() {
    }

    public DishHasDisposalPK(int dishDishId, int disposalDisposalId) {
        this.dishDishId = dishDishId;
        this.disposalDisposalId = disposalDisposalId;
    }

    @Column(name = "dish_dish_id", nullable = false)
    @Id
    public int getDishDishId() {
        return dishDishId;
    }

    public void setDishDishId(int dishDishId) {
        this.dishDishId = dishDishId;
    }

    @Column(name = "disposal_disposal_id", nullable = false)
    @Id
    public int getDisposalDisposalId() {
        return disposalDisposalId;
    }

    public void setDisposalDisposalId(int disposalDisposalId) {
        this.disposalDisposalId = disposalDisposalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DishHasDisposalPK that = (DishHasDisposalPK) o;

        if (dishDishId != that.dishDishId) return false;
        if (disposalDisposalId != that.disposalDisposalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dishDishId;
        result = 31 * result + disposalDisposalId;
        return result;
    }
}
