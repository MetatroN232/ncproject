package cafe.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "order_has_dish")
@IdClass(OrderHasDishPK.class)
public class OrderHasDish {
    private int orderOrderId;
    private int dishDishId;
    private int count;
    @JsonIgnore
    private Order orderByOrderOrderId;
    @JsonIgnore
    private Dish dishByDishDishId;

    public OrderHasDish() {
    }

    public OrderHasDish(int orderOrderId, int dishDishId, int count) {
        this.orderOrderId = orderOrderId;
        this.dishDishId = dishDishId;
        this.count = count;
    }

    @Id
    @Column(name = "order_order_id", nullable = false)
    public int getOrderOrderId() {
        return orderOrderId;
    }

    public void setOrderOrderId(int orderOrderId) {
        this.orderOrderId = orderOrderId;
    }

    @Id
    @Column(name = "dish_dish_id", nullable = false)
    public int getDishDishId() {
        return dishDishId;
    }

    public void setDishDishId(int dishDishId) {
        this.dishDishId = dishDishId;
    }

    @Basic
    @Column(name = "count", nullable = false)
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderHasDish that = (OrderHasDish) o;

        if (orderOrderId != that.orderOrderId) return false;
        if (dishDishId != that.dishDishId) return false;
        if (count != that.count) return false;

        return true;
    }

    @Override
    public String toString() {
        return "OrderHasDish{" +
                "orderOrderId=" + orderOrderId +
                ", dishDishId=" + dishDishId +
                ", count=" + count +
                '}';
    }

    @Override
    public int hashCode() {
        int result = orderOrderId;
        result = 31 * result + dishDishId;
        result = 31 * result + count;
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_order_id", referencedColumnName = "order_id", nullable = false, insertable = false, updatable = false)
    public Order getOrderByOrderOrderId() {
        return orderByOrderOrderId;
    }

    public void setOrderByOrderOrderId(Order orderByOrderOrderId) {
        this.orderByOrderOrderId = orderByOrderOrderId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dish_dish_id", referencedColumnName = "dish_id", nullable = false, insertable = false, updatable = false)
    public Dish getDishByDishDishId() {
        return dishByDishDishId;
    }

    public void setDishByDishDishId(Dish dishByDishDishId) {
        this.dishByDishDishId = dishByDishDishId;
    }
}
