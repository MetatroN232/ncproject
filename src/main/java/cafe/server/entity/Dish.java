package cafe.server.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Dish {
    private int dishId;
    private String name;
    private int categoryId;
    private Category categoryByCategoryId;
    private Collection<DishHasDisposal> dishHasDisposalsByDishId;
    private Collection<OrderHasDish> orderHasDishesByDishId;

    public Dish() {
    }

    public Dish(int dishId, String name, int categoryId) {
        this.dishId = dishId;
        this.name = name;
        this.categoryId = categoryId;
    }

    @Id
    @Column(name = "dish_id", nullable = false)
    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "category_id", nullable = false)
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (dishId != dish.dishId) return false;
        if (categoryId != dish.categoryId) return false;
        if (name != null ? !name.equals(dish.name) : dish.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dishId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + categoryId;
        return result;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "dishId=" + dishId +
                ", name='" + name + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "category_id", nullable = false, insertable = false, updatable = false)
    public Category getCategoryByCategoryId() {
        return categoryByCategoryId;
    }

    public void setCategoryByCategoryId(Category categoryByCategoryId) {
        this.categoryByCategoryId = categoryByCategoryId;
    }

    @OneToMany(mappedBy = "dishByDishDishId", fetch = FetchType.LAZY)
    public Collection<DishHasDisposal> getDishHasDisposalsByDishId() {
        return dishHasDisposalsByDishId;
    }

    public void setDishHasDisposalsByDishId(Collection<DishHasDisposal> dishHasDisposalsByDishId) {
        this.dishHasDisposalsByDishId = dishHasDisposalsByDishId;
    }

    @OneToMany(mappedBy = "dishByDishDishId", fetch = FetchType.LAZY)
    public Collection<OrderHasDish> getOrderHasDishesByDishId() {
        return orderHasDishesByDishId;
    }

    public void setOrderHasDishesByDishId(Collection<OrderHasDish> orderHasDishesByDishId) {
        this.orderHasDishesByDishId = orderHasDishesByDishId;
    }
}
