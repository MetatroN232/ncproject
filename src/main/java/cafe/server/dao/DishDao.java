package cafe.server.dao;

import cafe.server.entity.Dish;

import java.util.List;

public interface DishDao extends GenericDAO<Dish, Integer> {
    List<Dish> findByName(String name);
}
