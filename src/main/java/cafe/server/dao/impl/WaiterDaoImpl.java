package cafe.server.dao.impl;

import cafe.server.dao.AbstractGenericDAO;
import cafe.server.dao.WaiterDao;
import cafe.server.entity.Order;
import cafe.server.entity.Waiter;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class WaiterDaoImpl extends AbstractGenericDAO<Waiter, Integer> implements WaiterDao {
    @Override
    public List<Order> getOrdersByWaiter(Integer waiterId) {
        List<Order> result = null;

        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from Order where Order.waiterId = :waiterId");
                query.setParameter("waiterId", waiterId);
                result = query.getResultList();

                transaction.commit();
            } catch (HibernateException ex) {
                transaction.rollback();
            }
        }
        return result;
    }
}
