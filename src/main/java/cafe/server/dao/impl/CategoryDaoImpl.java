package cafe.server.dao.impl;

import cafe.server.dao.AbstractGenericDAO;
import cafe.server.dao.CategoryDao;
import cafe.server.entity.Category;
import cafe.server.entity.Dish;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CategoryDaoImpl extends AbstractGenericDAO<Category, Integer> implements CategoryDao {
    @Override
    public List<Dish> getDishesByCategory(int categoryId) {

        List<Dish> result = null;
        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();

            try {
                transaction.begin();

                Query query = session.createQuery("from Dish where categoryId = :categoryId");
                query.setParameter("categoryId", categoryId);
                result = query.getResultList();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
            return result;
        }
    }
}