package cafe.server.dao.impl;

import cafe.server.dao.AbstractGenericDAO;
import cafe.server.dao.DisposalDao;
import cafe.server.entity.Dish;
import cafe.server.entity.DishHasDisposal;
import cafe.server.entity.DishHasDisposalPK;
import cafe.server.entity.Disposal;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DisposalDaoImpl extends AbstractGenericDAO<Disposal, Integer> implements DisposalDao {

    private final DishHasDisposalDaoImpl priceDao = new DishHasDisposalDaoImpl();

    @Override
    public DishHasDisposal getActualPrice(int dishId) {
        DishHasDisposal result = null;

        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from DishHasDisposal dd join Disposal d on dd.disposalDisposalId = d.disposalId where dd.dishDishId = :dishId and d.date in (select max(d.date) from DishHasDisposal dd JOIN Disposal d ON dd.disposalDisposalId = d.disposalId where dd.dishDishId = :dishId)");
                query.setParameter("dishId", dishId);
                List<Object[]> list = query.getResultList();
                if (list.size() == 0) {
                    result = null;
                } else {
                    result = (DishHasDisposal) list.get(0)[0];
                }

                transaction.commit();
            } catch (HibernateException ex) {
                transaction.rollback();
            }
        }


        return result;
    }

    @Override
    public List<Dish> getDishes(int disposalId) {
        List<Dish> result = null;

        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from Dish as dish join DishHasDisposal dd on dish.dishId = dd.dishDishId where dd.disposalDisposalId = :orderId");
                query.setParameter("orderId", disposalId);
                List<Object[]> list = query.getResultList();

                result = new ArrayList<>();
                for (Object[] o : list) {
                    result.add((Dish) o[0]);
                }

                transaction.commit();
            } catch (HibernateException ex) {
                transaction.rollback();
            }
        }

        return result;
    }

    @Override
    public DishHasDisposal getPriceByDate(int dishId, Date date) {

        //TODO replace query

        DishHasDisposal result = null;

        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from DishHasDisposal dd join Disposal d on dd.disposalDisposalId = d.disposalId where d.date <= :date and dd.dishDishId = :dishId order by d.date desc")
                        .setParameter("date", date)
                        .setParameter("dishId", dishId);
                List resultList = query.getResultList();
                if (!resultList.isEmpty()) {
                    Object[] o = (Object[]) resultList.get(0);
                    if (o != null) {
                        result = (DishHasDisposal) o[0];
                    }
                }

                transaction.commit();
            } catch (HibernateException ex) {
                transaction.rollback();
                ex.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public DishHasDisposal getPriceByOrder(int dishId, int orderId) {
        DishHasDisposal result = null;

        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from DishHasDisposal dd join Disposal d on dd.disposalDisposalId = d.disposalId where d.date <= (select o.date from Order o where o.orderId = :orderId) and dd.dishDishId = :dishId order by d.date desc");
                query.setParameter("orderId", orderId);
                query.setParameter("dishId", dishId);
                List<Object[]> list = query.getResultList();
                result = (DishHasDisposal) list.get(0)[0];

                transaction.commit();
            } catch (HibernateException ex) {
                transaction.rollback();
            }
        }

        return result;
    }

    @Override
    public DishHasDisposal getPriceByDisposal(int disposalId, int dishId) {
        DishHasDisposal result = priceDao.find(new DishHasDisposalPK(dishId, disposalId));
        return result;
    }

    @Override
    public void savePrice(DishHasDisposal dishHasDisposal) {
        priceDao.save(dishHasDisposal);
    }

    @Override
    public void delete(Disposal disposal) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        try {
            Query query = session.createQuery("delete from DishHasDisposal dd where dd.dishDishId = :disposalId");
            query.setParameter("disposalId", disposal.getDisposalId());
            query.executeUpdate();

            query = session.createQuery("delete from Disposal where disposalId = :disposalId");
            query.setParameter("disposalId", disposal.getDisposalId());
            query.executeUpdate();

            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updatePrice(DishHasDisposal dishHasDisposal) {
        priceDao.update(dishHasDisposal);
    }

    @Override
    public void deletePrice(DishHasDisposal dishHasDisposal) {
        priceDao.delete(dishHasDisposal);
    }

    @Override
    public void clearDisposal(Disposal disposal) {
        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("delete from DishHasDisposal where disposalDisposalId = :disposalId");
                query.setParameter("disposalId", disposal.getDisposalId());
                query.executeUpdate();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }
    }

    private static class DishHasDisposalDaoImpl extends AbstractGenericDAO<DishHasDisposal, DishHasDisposalPK> {

    }
}
