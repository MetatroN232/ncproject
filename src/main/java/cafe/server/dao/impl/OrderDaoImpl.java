package cafe.server.dao.impl;

import cafe.server.dao.AbstractGenericDAO;
import cafe.server.dao.OrderDao;
import cafe.server.entity.Order;
import cafe.server.entity.OrderHasDish;
import cafe.server.entity.OrderHasDishPK;
import cafe.server.entity.Waiter;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class OrderDaoImpl extends AbstractGenericDAO<Order, Integer> implements OrderDao {

    private OrderHasDishDaoImpl orderDetailDao = new OrderHasDishDaoImpl();


    @Override
    public List<Order> getAll() {
        Session currentSession = getCurrentSession();
        Transaction tr = currentSession.getTransaction();
        List<Order> resultList = null;
        try {
            tr.begin();

            Query query = currentSession.createQuery("from Order order by date ");
            resultList = query.getResultList();

            tr.commit();
        } catch (HibernateException ex) {
            ex.printStackTrace();
            tr.rollback();
        } finally {
            currentSession.close();
        }
        return resultList;
    }

    @Override
    public Waiter getWaiterByOrderId(int orderId) {
        Waiter result = null;

        try (Session currentSession = getCurrentSession()) {
            Transaction transaction = currentSession.getTransaction();
            try {
                transaction.begin();
                Query query = currentSession.createQuery("from Waiter w where w.id = (select o.waiterId from Order o where o.id = :orderId)");
                query.setParameter("orderId", orderId);
                result = (Waiter) query.getSingleResult();
                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }

        return result;
    }

    @Override
    public List<OrderHasDish> getDishes(int orderId) {
        List<OrderHasDish> list = null;
        try (Session currentSession = getCurrentSession()) {
            Transaction transaction = currentSession.getTransaction();
            try {
                transaction.begin();

                Query query = currentSession
                        .createQuery("from OrderHasDish where orderOrderId = :orderId");
                query.setParameter("orderId", orderId);
                list = query.getResultList();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }

        }
        return list;
    }

    @Override
    public OrderHasDish getDishFromOrder(int orderId, int dishId) {
        OrderHasDish result = null;
        try (Session currentSession = getCurrentSession()) {
            Transaction transaction = currentSession.getTransaction();
            try {
                transaction.begin();
                Query query = currentSession.createQuery("from OrderHasDish od where od.dishDishId = :dishId and od.orderOrderId = :orderId");
                query.setParameter("dishId", dishId);
                query.setParameter("orderId", orderId);
                result = (OrderHasDish) query.getSingleResult();
                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }
        return result;
    }

    @Override
    public void saveOrderDetail(OrderHasDish orderHasDish) {
        orderDetailDao.save(orderHasDish);
    }

    @Override
    public void delete(Order order) {
        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        try {
            Query query = session.createQuery("delete from OrderHasDish od where od.orderOrderId = :orderId");
            query.setParameter("orderId", order.getOrderId());
            query.executeUpdate();

            query = session.createQuery("delete from Order where orderId = :orderId");
            query.setParameter("orderId", order.getOrderId());
            query.executeUpdate();

            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateOrderDetail(OrderHasDish orderHasDish) {
        orderDetailDao.update(orderHasDish);
    }

    @Override
    public void deleteOrderDetail(OrderHasDish orderHasDish) {
        orderDetailDao.delete(orderHasDish);
    }

    @Override
    public void clearOrder(Order order) {
        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("delete from OrderHasDish where orderOrderId = :orderId");
                query.setParameter("orderId", order.getOrderId());
                query.executeUpdate();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }
    }

    @Override
    public void setStatus(int orderId, boolean status) {
        try (Session session = getCurrentSession()) {
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("update Order set active = :active where orderId = :orderId")
                        .setParameter("active", status)
                        .setParameter("orderId", orderId);
                query.executeUpdate();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }
    }

    @Override
    public List<Order> ordersByMonth(int month, int year) {
        List<Order> result = null;
        try (Session session = getCurrentSession()) {

            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from Order where month(date) = :month and year(date) = :year")
                        .setParameter("month", month)
                        .setParameter("year", year);

                result = query.getResultList();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }

        return result;
    }

    @Override
    public List<Order> ordersByDay(Date date) {
        List<Order> result = null;
        try (Session session = getCurrentSession()) {

            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Query query = session.createQuery("from Order where day(date) = day(:date) and " +
                        "month(date) = month(:date) and year(date) = year(:date) ")
                        .setParameter("date", date);

                result = query.getResultList();

                transaction.commit();
            } catch (HibernateException ex) {
                ex.printStackTrace();
                transaction.rollback();
            }
        }

        return result;
    }


    private class OrderHasDishDaoImpl extends AbstractGenericDAO<OrderHasDish, OrderHasDishPK> {

    }
}
