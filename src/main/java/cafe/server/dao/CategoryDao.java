package cafe.server.dao;


import cafe.server.entity.Category;
import cafe.server.entity.Dish;

import java.util.List;

/**
 * Created by Arseniy on 27.01.2017.
 */
public interface CategoryDao extends GenericDAO<Category, Integer> {
    List<Dish> getDishesByCategory(int categoryId);
}
