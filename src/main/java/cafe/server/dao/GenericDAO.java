package cafe.server.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Arseniy on 27.01.2017.
 */
public interface GenericDAO<T, PK extends Serializable> {
    void save(T object);
    T find(PK id);
    void update(T object);
    void delete(T object);
    List<T> getAll();
    void saveOrUpdate(T object);
    List<T> filterAndSort(Map<String, Object> filterMap, String sort);
}
