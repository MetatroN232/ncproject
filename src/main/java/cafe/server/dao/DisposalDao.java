package cafe.server.dao;

import cafe.server.entity.Dish;
import cafe.server.entity.DishHasDisposal;
import cafe.server.entity.Disposal;

import java.util.Date;
import java.util.List;

public interface DisposalDao extends GenericDAO<Disposal, Integer> {
    List<Dish> getDishes(int disposalId);

    DishHasDisposal getActualPrice(int dishId);

    DishHasDisposal getPriceByDate(int dishId, Date date);

    DishHasDisposal getPriceByOrder(int dishId, int orderId);

    DishHasDisposal getPriceByDisposal(int disposalId, int dishId);

    void savePrice(DishHasDisposal dishHasDisposal);

    void updatePrice(DishHasDisposal dishHasDisposal);

    void deletePrice(DishHasDisposal dishHasDisposal);

    void clearDisposal(Disposal disposal);
}
