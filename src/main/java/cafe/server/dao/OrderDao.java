package cafe.server.dao;

import cafe.server.entity.Order;
import cafe.server.entity.OrderHasDish;
import cafe.server.entity.Waiter;

import java.util.Date;
import java.util.List;


public interface OrderDao extends GenericDAO<Order, Integer> {
    Waiter getWaiterByOrderId(int orderId);

    List<OrderHasDish> getDishes(int orderId);

    OrderHasDish getDishFromOrder(int orderId, int dishId);

    void saveOrderDetail(OrderHasDish orderHasDish);

    void updateOrderDetail(OrderHasDish orderHasDish);

    void deleteOrderDetail(OrderHasDish orderHasDish);

    void clearOrder(Order order);

    void setStatus(int orderId, boolean status);

    List<Order> ordersByMonth(int month, int year);

    List<Order> ordersByDay(Date date);

    //List<Order> filterAndSort(Map<String, Object> filterMap);
}
