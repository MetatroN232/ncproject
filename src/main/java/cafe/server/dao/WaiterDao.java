package cafe.server.dao;

import cafe.server.entity.Order;
import cafe.server.entity.Waiter;

import java.util.List;

public interface WaiterDao extends GenericDAO<Waiter, Integer> {
    List<Order> getOrdersByWaiter(Integer waiterId);
}
