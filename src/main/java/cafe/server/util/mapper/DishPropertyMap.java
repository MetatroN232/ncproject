package cafe.server.util.mapper;

import cafe.server.entity.wrapper.DishWrapper;
import cafe.shared.dto.DishDto;
import org.modelmapper.PropertyMap;

public class DishPropertyMap extends PropertyMap<DishWrapper, DishDto> {
    @Override
    protected void configure() {
        map().setName(source.getDish().getName());
        map().setCategoryId(source.getDish().getCategoryId());
        map().setDishId(source.getDish().getDishId());
        map().setPrice(source.getDishHasDisposal().getPrice());
    }
}
